/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys;

import java.util.Iterator;
import java.util.Stack;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
//import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.wb.swt.SWTResourceManager;

import versys.dialogs.ErrorDialog;
import versys.petrinet.Arc;
import versys.petrinet.CType;
import versys.petrinet.Component;
import versys.petrinet.Node;
import versys.petrinet.PNState;
import versys.petrinet.PetriNet;
import versys.petrinet.Place;
import versys.petrinet.Transition;

/**
 * Class that represents an editor tab in Versys. Each tab manages a PetriNet and a Canvas to draw the net on.
 * @author Michael Rücker
 *
 */
public class EditorTab {

	/**
	 * The font that is used to render text onto the canvas.
	 */
	public final static Font CANVAS_FONT = SWTResourceManager.getFont("Monospaced", 10, SWT.NORMAL);
	
	private static int s_numTabs = 1;
	private int m_id;
	
	private final Versys m_versys;
	private Canvas m_canvas;
	private RunLog m_runLog;
	private int m_logWidth;
	
	private SashForm m_sashForm;
	private Composite m_composite;
	private final TabItem m_tab;
	
	private PetriNet m_lastNet;
	private PetriNet m_currentNet;
	private Stack<PetriNet> m_undoStack;
	private Stack<PetriNet> m_redoStack;
	
	private Rectangle m_selectionRect;
	private Component m_arcStart;
	
	private boolean	m_shiftDown;
	private boolean 	m_leftMousedown;
	private boolean		m_rightMouseDown;
	private Point		m_selectionOrig;
	private Point		m_mousePos;	
	private Point		m_scrollReference;
	private Point		m_scrollTotal;
	private Component 	m_dragReference;
	private boolean	m_dragged;
	private double	m_zoomFactor = 1.0;
	
	private String	m_filePath;
	private boolean m_changed;
	
	/**
	 * Constructs a new EditorTab instance.
	 * @param versys the Versys instance this tab is a part of
	 * @param parent the TabFolder this tab is a part of
	 * @param id the tabs ID
	 */
	public EditorTab(Versys versys, TabFolder parent, String id) {
				
		m_tab = new TabItem(parent, SWT.NONE);
		m_tab.setText(id);
		
		m_versys = versys;
				
		m_currentNet = new PetriNet(id);
		
		
		m_composite = new Composite(m_tab.getParent(), SWT.NONE);
		m_composite.setLayout(new FillLayout());
		m_sashForm = new SashForm(m_composite, SWT.HORIZONTAL);				
		m_canvas = new Canvas(m_sashForm, SWT.DOUBLE_BUFFERED | SWT.NO_BACKGROUND);		
		
		m_tab.setControl(m_composite);
		
		init();
	}
	
	/**
	 * Constructs a new EditorTab instance which will contain a PetriNet parsed from the given VersysFile instance.
	 * @param versys the Versys instance this tab is a part of
	 * @param parent the TabFolder this tab is a part of
	 * @param vFile VersysFile which contains the PetriNet to be parsed
	 * @throws Exception If the VersysFile does not contain a valid Petri net
	 */
	public EditorTab(Versys versys, TabFolder parent, VersysFile vFile) throws Exception {

		m_versys = versys;
		
		m_tab = new TabItem(parent, SWT.NONE);
		
		try { 
			m_currentNet = vFile.parsePetriNet();
			
		} catch(Exception e) {
			dispose();
			throw e;
		}
		m_filePath = vFile.getFilePath();
		m_tab.setText(m_currentNet.getID());
		
		
		m_composite = new Composite(m_tab.getParent(), SWT.NONE);
		m_composite.setLayout(new FillLayout());
		m_sashForm = new SashForm(m_composite, SWT.HORIZONTAL);				
		m_canvas = new Canvas(m_sashForm, SWT.DOUBLE_BUFFERED | SWT.NO_BACKGROUND);		
		
		m_tab.setControl(m_composite);					
				
		init();
	}
	
	/**
	 * Constructs a new EditorTab instance which will contain a PetriNet parsed from the given VersysFile instance.
	 * @param versys the Versys instance this tab is a part of
	 * @param parent the TabFolder this tab is a part of
	 * @param vFile VersysFile which contains the PetriNet to be parsed
	 * @param id the PetriNet's ID will be set to this
	 * @throws Exception If the VersysFile does not contain a valid Petri net.
	 */
	public EditorTab(Versys versys, TabFolder parent, VersysFile vFile, String id) throws Exception {
		
		m_versys = versys;
		
		m_tab = new TabItem(parent, SWT.NONE);
	//	m_canvas = new Canvas(m_tab.getParent(), SWT.NONE);
		
		try {
			m_currentNet = vFile.parsePetriNet();
			
		} catch(Exception e) {
			dispose();
			throw e;
		}
		m_filePath = vFile.getFilePath();
		
		m_currentNet.setID(id);		
		m_tab.setText(id);		
		
		m_composite = new Composite(m_tab.getParent(), SWT.NONE);
		m_composite.setLayout(new FillLayout());
		m_sashForm = new SashForm(m_composite, SWT.HORIZONTAL);				
		m_canvas = new Canvas(m_sashForm, SWT.DOUBLE_BUFFERED | SWT.NO_BACKGROUND);
		
		m_tab.setControl(m_composite);	
						
		init();
	}
	
	/**
	 * Add listeners
	 */
	private void init() {

		m_logWidth = 130;
		
		m_id = s_numTabs++;		
		m_leftMousedown = false;
				
		m_mousePos = new Point(0, 0);
		m_selectionRect = new Rectangle(0, 0, 0, 0);
		
		m_undoStack = new Stack<>();
		m_redoStack = new Stack<>();
		
		m_tab.setText("*"+m_tab.getText());
		m_changed = true;
						
		m_canvas.setFont(CANVAS_FONT);
		
		m_sashForm.addListener(SWT.Resize, new Listener() {
			public void handleEvent(Event e) {
				if(m_runLog == null || m_runLog.isDisposed()) return;
				int width = m_tab.getParent().getBounds().width;
				m_sashForm.setWeights(new int[]{width-m_logWidth, m_logWidth+1}); // FIXME: This is slightly inaccurate!
				m_sashForm.layout();								
			}
		});
		
		m_canvas.addMouseMoveListener(new MouseMoveListener() {
			public void mouseMove(MouseEvent e) {
				m_mousePos.x = (int)(e.x*m_zoomFactor);
				m_mousePos.y = (int)(e.y*m_zoomFactor);
				
				if(m_leftMousedown && m_versys.getCurrentTool() == Tool.TOOLS_SELECT) {
					
					if(m_currentNet.hasSelected() && m_dragReference != null) {					
						m_currentNet.dragSelected(m_mousePos.x, m_mousePos.y, m_dragReference);
						m_dragged = true;
					} else {
						if(!m_shiftDown) m_currentNet.unselect();
						m_currentNet.selectComponents(updateSelectionRect());
					}					
					m_canvas.redraw();				
				}
				
				if(m_arcStart != null) 
					m_canvas.redraw();
				
				if(m_rightMouseDown) {
					int translateX = m_mousePos.x - m_scrollReference.x;
					int translateY = m_mousePos.y - m_scrollReference.y;
					m_scrollTotal.x += translateX;
					m_scrollTotal.y += translateY;
					m_scrollReference = new Point(m_mousePos.x, m_mousePos.y);
					
					m_currentNet.translate(translateX, translateY);
					m_canvas.redraw();
				}
			}
		});
		
		m_canvas.addKeyListener(new KeyListener() {
			
			@Override
			public void keyReleased(KeyEvent e) {
				switch (e.keyCode) {
				case SWT.SHIFT:
					m_shiftDown = false;
					break;
				default:
					break;
				}				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.keyCode) {
				case SWT.SHIFT:
					m_shiftDown = true;
					break;				
				case SWT.ESC:
					m_arcStart = null;
					m_canvas.redraw();
					break;
				case SWT.DEL:
					backupNet();
					if(m_currentNet.deleteSelected()) {
						netChanged();
						m_canvas.redraw();
					}
					break;				
				default:
					break;
				}				
			}
		});
		
		m_canvas.addPaintListener(new PaintListener() {
			public void paintControl(PaintEvent e) {
				
				// Get size of visible area at zoom factor 1.0				
				Rectangle viewRect	= getViewRect();
				
				// The offset does not interest us
				viewRect.x = 0;
				viewRect.y = 0;
				
				// Create off-screen image
				Image image = new Image(e.display, viewRect);
				GC gc = new GC(image);
				gc.setAntialias(SWT.ON);
				
				// Clear background
				gc.setBackground(SWTResourceManager.getColor(255, 255, 224));
				gc.fillRectangle(viewRect);
				
				// Draw arc indicator
				if(m_arcStart != null && m_mousePos != null) {
					gc.setAlpha(125);
					gc.setForeground(SWTResourceManager.getColor(0, 0, 0));
					gc.setLineWidth(2);
					gc.drawLine(m_arcStart.getX(), m_arcStart.getY(), m_mousePos.x, m_mousePos.y);
				}
									
				// Draw Petri net
				gc.setAlpha(255);
				m_currentNet.draw(gc);
								
				// Draw selection rectangle
				if(!updateSelectionRect().isEmpty()) {															
					gc.setForeground(SWTResourceManager.getColor(0, 0, 0));
					
					gc.setLineWidth(1);
					if(m_zoomFactor > 1.0) 
						gc.setLineWidth((int)m_zoomFactor);
											
					gc.drawRectangle(m_selectionRect);					
				} 
				
				// Draw off-screen image onto canvas
				Rectangle clientArea = m_canvas.getBounds();
				e.gc.drawImage(image, 0, 0, viewRect.width, viewRect.height, 0, 0, clientArea.width, clientArea.height);				
				
				// Clean up
				gc.dispose();
				image.dispose();
			}
		});
				
		m_canvas.addMouseListener(new MouseAdapter() {
			
			// Press of a mouse button
			public void mouseDown(MouseEvent e) {
			
				if(e.button != 1) {
					resetOperations();
				}
				
				switch(e.button) {
					case 1:
						leftMouseButtonDown(e);
						break;
					case 3:
						rightMouseButtonDown(e);
						break;
					default:
						break;
				}
			}			
			
			// Release of a mouse button
			public void mouseUp(MouseEvent e) {
				
				switch (e.button) {
					case 1:
						leftMouseButtonUp(e);
						break;
					case 3:
						rightMouseButtonUp(e);
						break;
					default:
						break;
				}
			}
			
			public void mouseDoubleClick(MouseEvent e) {
				
				Tool currentTool = m_versys.getCurrentTool();
				if(currentTool == Tool.TOOLS_SELECT || currentTool == Tool.TOOLS_PLACE || currentTool == Tool.TOOLS_TRANS ) {
					resetOperations();
					Component c = m_currentNet.checkSelect((int)(e.x*m_zoomFactor), (int)(e.y*m_zoomFactor));
					if(c != null) {
						m_leftMousedown = false;
						backupNet();
						if(c.openPropertiesDialog(m_canvas))
							netChanged();
					}
				}
			}
		});
	}
	
	/**
	 * Returns whether or not there are elements on the undo stack.
	 * @return {@code true} if the undo stack holds at least one element, {@code false} otherwise
	 */
	private boolean canUndo() {
		return (!m_rightMouseDown) && (m_undoStack.size() > 0) && (getState() == PNState.PN_STATE_EDIT);
	}
	
	/**
	 * Returns whether or not there are elements on the redo stack.
	 * @return {@code true} if the redo stack holds at least one element, {@code false} otherwise 
	 */
	private boolean canRedo() {
		return 	(!m_rightMouseDown) && (m_redoStack.size() > 0) && (getState() == PNState.PN_STATE_EDIT);
	}
	
	/**
	 * Pushes the current net onto the redo stack and pops the first element from the undo stack, which then becomes the current net.
	 * @see #m_undoStack
	 * @see #m_redoStack
	 */
	public void undo() {
		if(!canUndo()) return;
		m_redoStack.push(m_currentNet);
		m_currentNet = m_undoStack.pop();
		m_tab.setText("*"+m_currentNet.getID());
		m_canvas.redraw();
	}
	
	
	/**
	 * Pushes the current net onto the undo stack and pops the first element from the redo stack, which then becomes the current net.
	 * @see #m_undoStack
	 * @see #m_redoStack
	 */
	public void redo() {
		if(!canRedo()) return;
		m_undoStack.push(m_currentNet);
		m_currentNet = m_redoStack.pop();
		m_tab.setText("*"+m_currentNet.getID());
		m_canvas.redraw();
	}	
	
	/**
	 * Updates and returns the current selection rectangle
	 * @return updated selection rectangle
	 */
	private Rectangle updateSelectionRect() {
		
		m_selectionRect.x = 0;
		m_selectionRect.y = 0;
		m_selectionRect.width = 0;
		m_selectionRect.height = 0;
				
		if(m_selectionOrig == null) 
			return m_selectionRect;
				
		if(m_selectionOrig.x < m_mousePos.x) {
			m_selectionRect.x = m_selectionOrig.x;
			m_selectionRect.width = m_mousePos.x - m_selectionOrig.x;
		} else {
			m_selectionRect.x = m_mousePos.x;
			m_selectionRect.width = m_selectionOrig.x - m_mousePos.x;
		}
		if(m_selectionOrig.y < m_mousePos.y) {
			m_selectionRect.y = m_selectionOrig.y;
			m_selectionRect.height = m_mousePos.y - m_selectionOrig.y;
		} else {
			m_selectionRect.y = m_mousePos.y;
			m_selectionRect.height = m_selectionOrig.y - m_mousePos.y;
		}
		
		return m_selectionRect;
	}
	
	/**
	 * Returns the tab's PetriNet instance.
	 * @return the tab's PetriNet instance
	 */
	public PetriNet getPetriNet() {
		return m_currentNet;
	}

	/**
	 * Starts a manual simulation.
	 */
	public void startManualSimulation() {
			
		m_runLog = new RunLog(m_sashForm);
		m_runLog.addListener(SWT.Resize, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				m_logWidth = m_runLog.getBounds().width;			
			}
		});
		
		int width = m_tab.getParent().getBounds().width;
		m_sashForm.setWeights(new int[]{width-m_logWidth, m_logWidth+1}); // FIXME: This is slightly inaccurate!
		m_sashForm.layout();		
				
		m_currentNet.startSimulation();
		m_canvas.redraw();
	}
			
	/**
	 * Starts an automatic simulation.
	 * @param versys the {@link versys.Versys} instance that should display the simulations progress (usually the instance that contains this editor tab)
	 * @param num transition steps to be simulated
	 * @param delay delay between steps in milliseconds
	 * @see org.eclipse.swt.widgets.ProgressBar
	 */
	public void startAutoSimulation(Versys versys, int num, int delay) {
		
		if(m_currentNet.getState() != PNState.PN_STATE_MANUAL_SIM) 
			startManualSimulation();
				
		AutoSimulation autoSim = new AutoSimulation(m_canvas, m_runLog, m_currentNet, num, delay, versys, getID());
		Thread tSim = new Thread(autoSim);
		m_currentNet.startAutoSimulation();
		m_canvas.redraw();
		tSim.start();		
	}
	
	/**
	 * Stops a running simulation, manual and automatic, and returns the net to edit mode.
	 */
	public void stopSimulation() {
		
		if(m_runLog != null)
			m_runLog.dispose();		
		m_sashForm.layout();
		
		m_currentNet.stopSimulation();
		m_canvas.redraw();
	}
	
	
	/**
	 * Stops a running automatic simulation and returns to manual simulation.
	 */
	public void stopAutoSimulation() {
		m_currentNet.stopAutoSimulation();
		m_canvas.redraw();
	}
	

	/**
	 * Calls the Canvas's redraw.
	 * @see org.eclipse.swt.widgets.Canvas#redraw()
	 */
	public void redraw() {
		m_canvas.redraw();
	}
	
	/**
	 * Saves the Petri net as a PNG image.
	 * @param path file path where the image will be saved
	 */
	public void saveImage(String path) {
		if(path == null) return;
		
		Image image = new Image(m_canvas.getDisplay(), m_canvas.getSize().x, m_canvas.getSize().y);
		GC gc = new GC(m_canvas);
		gc.copyArea(image, 0, 0);
		ImageLoader imgLoader = new ImageLoader();
		imgLoader.data = new ImageData[] {image.getImageData()};
		imgLoader.save(path, SWT.IMAGE_PNG);
		gc.dispose();
	}
	
	/**
	 * Saves the Petri net as an XML file.
	 * @param path file path where the net will be saved
	 */
	public void save(String path) {
		m_filePath = path;
		new VersysFile(m_currentNet).saveToXML(path);	
		m_changed = false;
		m_tab.setText(m_currentNet.getID());
	}
	
	/**
	 * Returns the file path of the Petri net or null if the net has not been saved. Loaded nets will have the file path of the loaded file.
	 * @return the file path of the Petri net or null if no path has been specified yet
	 */
	public String getFilePath() {
		return m_filePath;
	}
	
	/**
	 * Returns the Petri net's ID.
	 * @return the Petri net's ID
	 * @see PetriNet#getID()
	 */
	public String getName() {
		return m_currentNet.getID();
	}
	
	/**
	 * Returns the editor tab's ID.
	 * @return the editor tab's ID
	 */
	public int getID() {
		return m_id;
	}
	
	/**
	 * Sets the Petri net's ID.
	 * @param name new ID of the Petri net
	 * @see PetriNet#setID(String)
	 */
	public void setName(String name) {
		backupNet();
		m_currentNet.setID(name);
		m_tab.setText(name);
		netChanged();
	}

	/**
	 * Calls the Canvas's and the TabItem's dispose method.
	 * @see Canvas#dispose()
	 */
	public void dispose() {
		if(m_currentNet != null) stopSimulation();
		if(m_canvas != null) m_canvas.dispose();		
		if(m_tab != null) m_tab.dispose();		
	}
	
	/**
	 * Returns the default name for the next new tab which will be 'Petrinetz x' where x is the running number of tabs.
	 * @return the default name for the next new tab
	 */
	public static String getDefaultName() {
		return "Petrinetz " + s_numTabs;
	}
	
	/**
	 * Returns true if the Petri net has been changed since the last time it was saved or if the net has not been saved yet at all.
	 * @return true if the net has been changed since the last time it was saved, false otherwise
	 */
	public boolean isChanged() {
		return m_changed;
	}
	
	/**
	 * Makes a copy of the current net. Only one backup copy is maintained. Subsequent calls to this method will override any previously made copy.
	 * @see #netChanged()
	 */
	public void backupNet() {		
		try {
			m_lastNet = m_currentNet.copy();
		} catch(Exception e) {
			e.printStackTrace();
			new ErrorDialog(m_canvas.getShell(), "Ein Fehler ist aufgetreten.", e).open();
		}
	}
	
	/**
	 * Notifies the Tab that the PeriNet has been changed, causing it to change its label and to push the last backup copy of the net onto the undo stack.
	 * @see #backupNet()
	 */
	public void netChanged() {
		if(!m_changed) m_changed = true;						
		
		m_tab.setText("*"+m_currentNet.getID());
		m_undoStack.push(m_lastNet);
		m_redoStack.clear();
		m_canvas.redraw();
	}
	

	/**
	 * Aborts and resets all current editing operations.
	 */
	public void resetOperations() {
		m_currentNet.unselect();
		m_arcStart = null;
	}
	
	/**
	 * Returns the current state of the Petri net.
	 * @return the Petri net's current state
	 * @see PNState
	 */
	public PNState getState() {
		return m_currentNet.getState();
	}
	
	/**
	 * Processes the press of the left mouse button on the editor. This method is pretty convoluted
	 * since pretty much everything in this editor is done via left clicks. Maybe someone should refactor
	 * this at some point.
	 * @param e the MouseEvent
	 */
	private void leftMouseButtonDown(MouseEvent e) {
		m_leftMousedown = true;
		
		Point pointer = new Point((int)(e.x*m_zoomFactor), (int)(e.y*m_zoomFactor));
		
		Component c;
		switch (m_versys.getCurrentTool()) {
		
		case TOOLS_SELECT:
								
			backupNet();
			m_dragged = false;					
			
			c = m_currentNet.checkSelect(pointer.x, pointer.y);
			
			// This code is a complete fucking zoo!
			if(m_currentNet.getSelected().size() > 1) {						
				if(c == null) {
					if(!m_shiftDown) m_currentNet.unselect();
					m_selectionOrig = new Point(pointer.x, pointer.y);
					m_mousePos.x = pointer.x;
					m_mousePos.y = pointer.y;
				} else if(m_shiftDown) {
					m_currentNet.toggleSelection(c);
				} else {
					if(!c.isSelected()) {
						m_currentNet.unselect();
						m_currentNet.toggleSelection(c);
					} 
					m_dragReference = c;
				}

			} else {
				if(!m_shiftDown) 
					m_currentNet.unselect();						
				
				if(c != null) { 
					m_currentNet.toggleSelection(c);
					m_dragReference = c;
				} else {
					m_selectionOrig = new Point(pointer.x, pointer.y);
					m_mousePos.x = pointer.x;
					m_mousePos.y = pointer.y;	
				}
			}									
			break;
			
		case TOOL_DELETE:
			m_currentNet.unselect();
			c = m_currentNet.checkSelect(pointer.x, pointer.y);
			if(c != null) {
				backupNet();
				m_currentNet.deleteComponent(c);
				netChanged();
			}
			break;
			
		case TOOLS_PLACE:
			c = m_currentNet.checkSelect(pointer.x, pointer.y);
			if(c != null) break;
			
			backupNet();
			Place p = new Place(pointer.x, pointer.y);
			new versys.petrinet.Label(p);
			m_currentNet.addComponent(p, false);		
			netChanged();
			break;
			
		case TOOLS_TRANS:
			c = m_currentNet.checkSelect(pointer.x, pointer.y);
			if(c != null) break;
			
			backupNet();
			Transition t = new Transition(pointer.x, pointer.y);
			new versys.petrinet.Label(t);
			m_currentNet.addComponent(t, false);
			netChanged();
			break;
			
		case TOOLS_ARCS:
			c = m_currentNet.checkSelect(pointer.x, pointer.y);					
			if(c != null) {						
				
				CType type = c.getType();						
				
				if(m_arcStart != null) {
					if(m_arcStart.getType() == CType.CTYPE_PLACE && type == CType.CTYPE_TRANS || m_arcStart.getType() == CType.CTYPE_TRANS && type == CType.CTYPE_PLACE) {								
						backupNet();
						Arc a = new Arc((Node)m_arcStart, (Node)c);
						new versys.petrinet.Label(a);
						m_currentNet.addComponent(a, false);								
						m_arcStart = null;
						netChanged();
					}
				} else if(type == CType.CTYPE_PLACE || type == CType.CTYPE_TRANS) {
					m_arcStart = c;
				}
			}
			break;				
			
		case TOOLS_MANUAL_SIM:
			c = m_currentNet.checkSelect(pointer.x, pointer.y);					
			if(c != null && c.getType() == CType.CTYPE_TRANS && ((Transition)c).isEnabled()) {
				try{ 
					((Transition)c).fire();
					m_runLog.log( c.getLabel().getText() );
				}
				catch(Exception ex) {		
					ex.printStackTrace();
					new ErrorDialog(m_canvas.getShell(), ex.getMessage(), ex).open();
				}
			}						
			break;

		default: break;
		}
		m_canvas.redraw();
	}
	
	/**
	 * Processes the release of the left mouse button on the editor.
	 * @param e the MouseEvent
	 */
	private void leftMouseButtonUp(MouseEvent e) {
		m_leftMousedown = false;
		m_selectionOrig = null;
		
		if(m_dragged) 
			netChanged();				
		else if (m_dragReference != null && !m_shiftDown) {
			m_currentNet.unselect();
			m_currentNet.toggleSelection(m_dragReference);
		}
		
		m_dragReference = null;
		m_canvas.redraw();		
	}
	
	/**
	 * Processes the press of the right mouse button on the editor.
	 * @param e the MouseEvent
	 */
	private void rightMouseButtonDown(MouseEvent e) {
		m_rightMouseDown = true;
		m_scrollReference = new Point((int)(e.x*m_zoomFactor), (int)(e.y*m_zoomFactor));
		m_scrollTotal = new Point(0,0);
	}
	
	/**
	 * Processes the release of the right mouse button on the editor.
	 * @param e the MouseEvent
	 */
	private void rightMouseButtonUp(MouseEvent e) {
		m_rightMouseDown = false;
		
		translateUndoRedoStacks(m_scrollTotal.x, m_scrollTotal.y);
	}
	
	/**
	 * Increases the zoom level, up to a maximum of 250%. 
	 */
	public void zoomIn() {
		if(m_zoomFactor > .4)
			setZoomFactor(m_zoomFactor-0.1);	
	}
	
	/**
	 * Returns the coordinates of the current mouse cursor position
	 * @return mouse cursor position
	 */
	public Point getMousePos() {
		return m_mousePos;
	}
	
	/**
	 * Decreases the zoom level, down to a minimum of 33%.
	 */
	public void zoomOut() {
		if(m_zoomFactor < 3.0) 
			setZoomFactor(m_zoomFactor+0.1);
	}
	
	/**
	 * Resets the zoom level to 100%.
	 */
	public void resetZoom() {
		setZoomFactor(1.0);		
	}

	/**
	 * Sets the zoom factor to the passed value.
	 * @param zoomFactor the new zoom factor (0, inf]
	 */
	private void setZoomFactor(double zoomFactor) {
		Rectangle currViewRect = getViewRect();
		m_zoomFactor = zoomFactor;
		Rectangle newViewRect = getViewRect();
		translateNets(currViewRect.x-newViewRect.x, currViewRect.y-newViewRect.y);
		m_canvas.redraw();
	}
	
	/**
	 * Translates all Petri nets, i.e. the current net as well as all nets on the
	 * undo and redo stacks according to the passes parameters
	 * @param x x translation
	 * @param y y translation
	 */
	private void translateNets(int x, int y) {
		m_currentNet.translate(x, y);
		translateUndoRedoStacks(x, y);
	}
	
	/**
	 * Translates the Petri nets on the undo and redo stack according to the passed
	 * parameters.
	 * @param x x translation
	 * @param y y translation
	 */
	private void translateUndoRedoStacks(int x, int y) {
		Iterator<PetriNet> pnit = m_undoStack.iterator();
		while(pnit.hasNext()) {
			pnit.next().translate(x, y);
		}
		
		pnit = m_redoStack.iterator();
		while(pnit.hasNext()) {
			pnit.next().translate(x, y);
		}
	}
	
	/**
	 * Returns a Rectangle representing the visible editor area at a zoom factor of 1.0.
	 * @return a rectangle representing the visible editor area at a zoom factor of 1.0
	 */
	private Rectangle getViewRect() {
		Rectangle canvasBounds = m_canvas.getBounds();
		double viewWidth 	= canvasBounds.width*m_zoomFactor;
		double viewHeight	= canvasBounds.height*m_zoomFactor;
		double viewX		= canvasBounds.x + (canvasBounds.width-viewWidth)/2.0;
		double viewY		= canvasBounds.y + (canvasBounds.height-viewHeight)/2.0;
		return new Rectangle((int)viewX, (int)viewY, (int)viewWidth, (int)viewHeight);
	}
	
}
