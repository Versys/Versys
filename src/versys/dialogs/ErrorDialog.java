/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

import versys.Versys;

/**
 * This is not the message you are looking for.
 * @author Michael Rücker
 */
public class ErrorDialog extends Dialog {

	private Object m_result;
	private Shell m_shlError;
	
	private final String m_msg;
	private final Exception m_exception;

	/**
	 * Create the dialog.
	 * @param parent the parent shell
	 * @param msg the error message
	 * @param e the exception that caused the error
	 */
	public ErrorDialog(Shell parent, String msg, Exception e) {
		super(parent, SWT.DIALOG_TRIM);
		getParent().setEnabled(false);
		
		m_msg = msg;
		m_exception = e;
	}

	/**
	 * Open the dialog.
	 * @return the result, which is always null
	 */
	public Object open() {		
		m_exception.printStackTrace(System.out);
		
		createContents();
		m_shlError.open();
		m_shlError.layout();
		Display display = getParent().getDisplay();
		while (!m_shlError.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return m_result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		m_shlError = new Shell(getParent(), getStyle());
		m_shlError.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		m_shlError.setSize(404, 140);
		m_shlError.setText("Fehler");
		
		Label lblEmblem = new Label(m_shlError, SWT.NONE);
		lblEmblem.setImage(SWTResourceManager.getImage(ErrorDialog.class, Versys.ASSETS+"emblem-important-4.png"));
		lblEmblem.setBounds(10, 10, 48, 48);
		
		Button btnOk = new Button(m_shlError, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_shlError.dispose();
			}
		});
		btnOk.setBounds(300, 76, 86, 27);
		btnOk.setText("OK");
		
		StyledText styledText = new StyledText(m_shlError, SWT.MULTI | SWT.WRAP);
		styledText.setBackground(m_shlError.getBackground());
		styledText.setEditable(false);
		styledText.setText(m_msg + "\n"+m_exception.getClass().getName()+": "+m_exception.getMessage());
		styledText.setBounds(78, 10, 308, 60);
	}
}
