/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;

/**
 * Because every team needs a name! And so does your Petri net.
 * @author Michael Rücker
 *
 */
public class NamingDialog extends Dialog {

	private String m_result;
	private Shell m_shlNeuesPetrinetz;
	private Text m_text;

	private final String m_defaultName;
	/**
	 * Create the dialog.
	 * @param parent the parent shell
	 * @param name the net's default name that is shown in the text field
	 */
	public NamingDialog(Shell parent, String name) {
		super(parent, SWT.DIALOG_TRIM);
		getParent().setEnabled(false);
		
		m_defaultName = name;
		m_result = "";
	}

	/**
	 * Open the dialog.
	 * @return the resulting name as a String
	 */
	public String open() {
		
		createContents();
		m_shlNeuesPetrinetz.open();
		m_shlNeuesPetrinetz.layout();
		Display display = getParent().getDisplay();
		while (!m_shlNeuesPetrinetz.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return m_result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		m_shlNeuesPetrinetz = new Shell(getParent(), getStyle());
		m_shlNeuesPetrinetz.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		m_shlNeuesPetrinetz.setSize(303, 110);
		m_shlNeuesPetrinetz.setText("Petrinetz benennen");
		
		Label lblName = new Label(m_shlNeuesPetrinetz, SWT.NONE);
		lblName.setBounds(10, 10, 43, 17);
		lblName.setText("Name:");
		
		m_text = new Text(m_shlNeuesPetrinetz, SWT.BORDER);
		m_text.setBounds(59, 10, 226, 27);
		m_text.setText(m_defaultName);
		
		Button btnAbbrechen = new Button(m_shlNeuesPetrinetz, SWT.NONE);
		btnAbbrechen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_shlNeuesPetrinetz.dispose();
			}
		});
		btnAbbrechen.setBounds(10, 43, 86, 27);
		btnAbbrechen.setText("Abbrechen");
		
		Button btnOk = new Button(m_shlNeuesPetrinetz, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_result = m_text.getText();
				m_shlNeuesPetrinetz.dispose();
			}
		});
		btnOk.setBounds(201, 43, 86, 27);
		btnOk.setText("OK");

	}
}
