/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import versys.EditorTab;
import versys.Versys;

import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

/**
 * This dialog shows up if you try to close an unsaved net. You sure you want to throw away all your hard modeling work?
 * @author Michael Rücker
 *
 */
public class UnsavedChangesDialog extends Dialog {

	private boolean m_result;
	private Shell m_shlUnsavedChanges;

	private final EditorTab m_editorTab;
	
	/**
	 * Create the dialog
	 * @param parent the parent shell
	 * @param eTab the EditorTab of the net
	 * @see EditorTab
	 */
	public UnsavedChangesDialog(Shell parent, EditorTab eTab) {
		super(parent, SWT.DIALOG_TRIM);
		m_editorTab = eTab;
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public boolean open() {
		getParent().setEnabled(false);
		createContents();
		m_shlUnsavedChanges.open();
		m_shlUnsavedChanges.layout();
		Display display = getParent().getDisplay();
		while (!m_shlUnsavedChanges.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return m_result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		m_shlUnsavedChanges = new Shell(getParent(), getStyle());
		m_shlUnsavedChanges.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		m_shlUnsavedChanges.setSize(454, 140);
		m_shlUnsavedChanges.setText("Änderungen Speichern?");
		
		Label label = new Label(m_shlUnsavedChanges, SWT.NONE);
		label.setImage(SWTResourceManager.getImage(UnsavedChangesDialog.class, Versys.ASSETS+"emblem-important-3.png"));
		label.setBounds(10, 10, 64, 64);
		
		Label lblNewLabel = new Label(m_shlUnsavedChanges, SWT.WRAP);
		lblNewLabel.setFont(SWTResourceManager.getFont("Sans", 10, SWT.BOLD));
		lblNewLabel.setBounds(104, 13, 330, 38);
		lblNewLabel.setText("Änderungen an '"+m_editorTab.getName()+"' speichern?");
		
		Button btnSave = new Button(m_shlUnsavedChanges, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String path = m_editorTab.getFilePath();
				if(path == null) {
					FileDialog saveDialog = new FileDialog(m_shlUnsavedChanges, SWT.SAVE);
					saveDialog.setFilterExtensions(new String[] {"*.xml"});
					saveDialog.setFileName(m_editorTab.getName());					
					path = saveDialog.open();
				}
				if(path != null) m_editorTab.save(path);
				m_result = true;
				m_shlUnsavedChanges.dispose();
			}
		});
		btnSave.setBounds(348, 80, 86, 27);
		btnSave.setText("Speichern");
		
		Button btnNewButton = new Button(m_shlUnsavedChanges, SWT.NONE);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_result = true;
				m_shlUnsavedChanges.dispose();
			}
		});
		btnNewButton.setBounds(230, 80, 112, 27);
		btnNewButton.setText("Nicht Speichern");
		
		Button btnNewButton_1 = new Button(m_shlUnsavedChanges, SWT.NONE);
		btnNewButton_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_result = false;
				m_shlUnsavedChanges.dispose();
			}
		});
		btnNewButton_1.setBounds(10, 80, 86, 27);
		btnNewButton_1.setText("Abbrechen");
	}
}
