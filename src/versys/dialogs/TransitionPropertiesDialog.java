/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import versys.petrinet.Transition;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;

/**
 * Not much to do here. Change the transitions name.
 * @author Michael Rücker
 *
 */
public class TransitionPropertiesDialog extends Dialog {

	private boolean result;
	private Shell shlTransitionEigenschaften;

	private final Transition m_transition;
	private Text textName;
	private Button btnBezeichnungAnzeigen;
	
	/**
	 * Create the dialog.
	 * @param parent the parent shell
	 * @param t the transition, whose properties this is all about
	 */
	public TransitionPropertiesDialog(Shell parent, Transition t) {
		super(parent, SWT.DIALOG_TRIM);		
		getParent().setEnabled(false);
		
		m_transition = t;
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public boolean open() {
		createContents();
		shlTransitionEigenschaften.open();
		shlTransitionEigenschaften.layout();
		Display display = getParent().getDisplay();
		while (!shlTransitionEigenschaften.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlTransitionEigenschaften = new Shell(getParent(), getStyle());
		shlTransitionEigenschaften.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		shlTransitionEigenschaften.setSize(291, 160);
		shlTransitionEigenschaften.setText("Transition - Eigenschaften");
		
		Label lblBeschriftung = new Label(shlTransitionEigenschaften, SWT.NONE);
		lblBeschriftung.setText("Beschriftung:");
		lblBeschriftung.setBounds(10, 10, 87, 17);
		
		textName = new Text(shlTransitionEigenschaften, SWT.BORDER);			
		textName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode == '\r') OK();
			}
		});
		textName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				textName.selectAll();
			}
		});
		textName.setText(m_transition.getLabel().getText());
		textName.setBounds(103, 10, 169, 27);
		
		btnBezeichnungAnzeigen = new Button(shlTransitionEigenschaften, SWT.CHECK);
		btnBezeichnungAnzeigen.setBounds(10, 53, 227, 21);
		btnBezeichnungAnzeigen.setText("Bezeichnung anzeigen");
		btnBezeichnungAnzeigen.setSelection(m_transition.isLabelVisible());
		
		Button btnOk = new Button(shlTransitionEigenschaften, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				OK();
			}
		});
		btnOk.setBounds(186, 96, 86, 27);
		btnOk.setText("OK");
		
		Button btnAbbrechen = new Button(shlTransitionEigenschaften, SWT.NONE);
		btnAbbrechen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = false;
				shlTransitionEigenschaften.dispose();
			}
		});
		btnAbbrechen.setBounds(10, 96, 86, 27);
		btnAbbrechen.setText("Abbrechen");

	}
	
	private void OK() {
		String label = textName.getText();
		if(label.length() > 0) {
			m_transition.getLabel().setText(label);
		}
		m_transition.setLabelVisible(btnBezeichnungAnzeigen.getSelection());
		result = true;
		shlTransitionEigenschaften.dispose();
	}
}
