/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import versys.Versys;

public class AboutDialog extends Dialog {

	private Object result;
	private Shell shlberVersys;

	/**
	 * Create the dialog.
	 * @param parent the dialog's parent shell
	 */
	public AboutDialog(Shell parent) {
		super(parent, SWT.DIALOG_TRIM);
		getParent().setEnabled(false);
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shlberVersys.open();
		shlberVersys.layout();
		Display display = getParent().getDisplay();
		while (!shlberVersys.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlberVersys = new Shell(getParent(), getStyle());
		shlberVersys.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		shlberVersys.setSize(395, 202);
		shlberVersys.setText("Über Versys");
		
		Label lblVersysVerteilte = new Label(shlberVersys, SWT.NONE);
		lblVersysVerteilte.setFont(SWTResourceManager.getFont("Sans", 12, SWT.BOLD));
		lblVersysVerteilte.setBounds(10, 10, 365, 19);
		lblVersysVerteilte.setText("Versys - Verteilte Systeme in der Schule");
		
		Label lblVersion = new Label(shlberVersys, SWT.NONE);
		lblVersion.setFont(SWTResourceManager.getFont("Sans", 10, SWT.BOLD));
		lblVersion.setBounds(192, 66, 68, 17);
		lblVersion.setText("Version:");
		
		Label lblAutor = new Label(shlberVersys, SWT.NONE);
		lblAutor.setFont(SWTResourceManager.getFont("Sans", 10, SWT.BOLD));
		lblAutor.setBounds(192, 43, 68, 17);
		lblAutor.setText("Autor:");
		
		Label lblNewLabel = new Label(shlberVersys, SWT.NONE);
		lblNewLabel.setAlignment(SWT.RIGHT);
		lblNewLabel.setBounds(266, 66, 99, 17);
		lblNewLabel.setText(Versys.MajorVersion+"."+Versys.MinorVersion+"."+Versys.RevisionVersion);
		
		Label lblLizenz = new Label(shlberVersys, SWT.NONE);
		lblLizenz.setFont(SWTResourceManager.getFont("Sans", 10, SWT.BOLD));
		lblLizenz.setBounds(192, 115, 68, 17);
		lblLizenz.setText("Lizenz:");
		
		Link link = new Link(shlberVersys, SWT.NONE);
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Program.launch("http://openiconlibrary.sourceforge.net/sources.html");
			}
		});
		link.setBounds(266, 138, 99, 17);
		link.setText("Icons by <a>oxygen</a>");
		
		Label lblNewLabel_1 = new Label(shlberVersys, SWT.NONE);
		lblNewLabel_1.setImage(SWTResourceManager.getImage(AboutDialog.class, Versys.ASSETS+"versys-logo.png"));
		lblNewLabel_1.setBounds(20, 35, 150, 120);
		
		Link link_1 = new Link(shlberVersys, SWT.NONE);
		link_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Program.launch("http://www.gnu.org/licenses/gpl.txt");
			}
		});
		link_1.setBounds(325, 115, 80, 17);
		link_1.setText("<a>GPLv3</a>");
		
		Link link_2 = new Link(shlberVersys, SWT.NONE);
		link_2.setBounds(275, 43, 90, 15);
		link_2.setText("<a>Michael Rücker</a>");
		link_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Program.launch("https://cses.informatik.hu-berlin.de/members/michael.ruecker/");
			}
		});

	}
}
