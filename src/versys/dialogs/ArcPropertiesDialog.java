/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.wb.swt.SWTResourceManager;

import versys.Versys;
import versys.petrinet.Arc;
import versys.petrinet.Component;
import versys.petrinet.PetriNet;


public class ArcPropertiesDialog extends Dialog {
	
	private boolean result;
	private Shell shlKanteEigenschaften;
	
	private final Arc m_arc;
	private final boolean[] m_universe;
	
	private final Spinner[] m_spinners;

	private ToolTip tipVarLabels;
	
	private Button	btnResetCurve;

	private final Image[] m_images;

	/**
	 * Create the dialog.
	 * @param parent the dialog's parent shell
	 * @param a the arc whose properties should be set
	 */
	public ArcPropertiesDialog(Shell parent, Arc a, boolean[] universe) {
		super(parent, SWT.DIALOG_TRIM);
		getParent().setEnabled(false);
		
		m_arc = a;
		m_universe = universe.clone();
		m_spinners = new Spinner[PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS];
		
		m_images = new Image[PetriNet.TOKEN_NUM_TYPES];
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			ImageData id = new ImageData(20, 20, 4, Component.s_paletteData);
			m_images[i] = new Image(Display.getDefault(), id);
			GC gc = new GC(m_images[i]);
			gc.setBackground(Component.s_tokenColors[i]);
			gc.fillRectangle(0, 0, 20, 20);
			gc.dispose();
		}
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public boolean open() {
		createContents();
		shlKanteEigenschaften.open();
		shlKanteEigenschaften.layout();
		Display display = getParent().getDisplay();
		while (!shlKanteEigenschaften.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			m_images[i].dispose();
		}
		
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlKanteEigenschaften = new Shell(getParent(), getStyle());
		shlKanteEigenschaften.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		shlKanteEigenschaften.setSize(394, 275);
		shlKanteEigenschaften.setText("Kante - Eigenschaften");
		
		Button btnOk = new Button(shlKanteEigenschaften, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int[] weights = new int[PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS];
				for(int i=0; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
					if(m_spinners[i] != null) {
						weights[i] = m_spinners[i].getSelection();
					}
				}				
				m_arc.setArcWeights(weights);
				if(btnResetCurve.getSelection()) m_arc.resetCurve();
				result = true;
				shlKanteEigenschaften.dispose();
			}
		});
		btnOk.setBounds(292, 208, 86, 27);
		btnOk.setText("OK");
		
		Button btnAbbrechen = new Button(shlKanteEigenschaften, SWT.NONE);
		btnAbbrechen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = false;
				shlKanteEigenschaften.dispose();
			}
		});
		btnAbbrechen.setBounds(10, 208, 86, 27);
		btnAbbrechen.setText("Abbrechen");
		
		btnResetCurve = new Button(shlKanteEigenschaften, SWT.CHECK);
		btnResetCurve.setBounds(10, 184, 336, 21);
		btnResetCurve.setText("Kurvenverlauf zurücksetzen");
		btnResetCurve.setSelection(false);
		btnResetCurve.setEnabled(m_arc.isCurved());

						
		Label lblKantengewichte = new Label(shlKanteEigenschaften, SWT.NONE);
		lblKantengewichte.setFont(SWTResourceManager.getFont("Sans", 10, SWT.NORMAL));
		lblKantengewichte.setBounds(9, 10, 178, 17);
		lblKantengewichte.setText("Kantengewichte:");
		
		Composite composite = new Composite(shlKanteEigenschaften, SWT.BORDER);
		composite.setBounds(10, 33, 368, 145);
		
		// Constant arc labelings
		Label lblKonstantesKantengewicht = new Label(composite, SWT.NONE);
		lblKonstantesKantengewicht.setBounds(0, 0, 67, 17);
		lblKonstantesKantengewicht.setText("Konstant");

		Table tblConstWeights = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tblConstWeights.setBounds(0, 23, 190, 120);
		tblConstWeights.setHeaderVisible(true);
		tblConstWeights.setLinesVisible(true);
		
		TableColumn tblclmnColor = new TableColumn(tblConstWeights, SWT.NONE);
		tblclmnColor.setWidth(86);
		tblclmnColor.setText("Farbe");
		
		TableColumn tblclmnColorAmount = new TableColumn(tblConstWeights, SWT.NONE);
		tblclmnColorAmount.setWidth(85);
		tblclmnColorAmount.setText("Anzahl");
		
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			if(m_universe[i]) {
				TableItem item = new TableItem(tblConstWeights, SWT.NONE);
				
				item.setImage(0, m_images[i]);
				try{ item.setText(0, Component.s_tokenIDs[i]); }
				catch(Exception e) {e.printStackTrace();}
				
				TableEditor editor = new TableEditor(tblConstWeights);
				m_spinners[i] = new Spinner(tblConstWeights, SWT.BORDER);
				m_spinners[i].setMaximum(9999);
				m_spinners[i].setSelection(m_arc.getArcWeights()[i]);
				editor.grabHorizontal = true;
				editor.setEditor(m_spinners[i], item, 1);		
			}
		}
		tblclmnColorAmount.pack();

		Table tblVarWeights = new Table(composite, SWT.BORDER | SWT.FULL_SELECTION);
		tblVarWeights.setBounds(196, 23, 170, 120);
		tblVarWeights.setHeaderVisible(true);
		tblVarWeights.setLinesVisible(true);
		
		TableColumn tblclmnVar = new TableColumn(tblVarWeights, SWT.NONE);
		tblclmnVar.setWidth(62);
		tblclmnVar.setText("Variable");
		
		TableColumn tblclmnVarFactor = new TableColumn(tblVarWeights, SWT.NONE);
		tblclmnVarFactor.setWidth(100);
		tblclmnVarFactor.setText("Anzahl");
		
		// Variable Arc Labelings
		Label lblVariablesKantengewicht = new Label(composite, SWT.NONE);
		lblVariablesKantengewicht.setBounds(196, 0, 53, 17);
		lblVariablesKantengewicht.setText("Variabel");

		CLabel helpLabel = new CLabel(composite, SWT.NONE);
		helpLabel.setBounds(255, 0, 22, 18);
		tipVarLabels = new ToolTip(helpLabel.getShell(), SWT.BALLOON | SWT.ICON_INFORMATION);
		tipVarLabels.setText("Variables Kantengewicht");
		tipVarLabels.setMessage(	// TODO: find a better explanation for variable arc labels!
			"Bei variablen Kantengewichten steht je eine Variable für\n" +
			"eine Markenfarbe. Eine Kante mit dem Gewicht 2X und 3Y \n" +
			"könnte z.B. 2 schwarze und 3 rote Marken oder auch 5 blaueb \n" +
			"Marken transportieren.\n" +
			"Hat eine Transition mehrere eingehende Kanten mit vari-\n" +
			"ablen Kantengewichten, dann steht z.B. eine Variable X\n" +
			"an allen Kanten für dieselben Markenfarbe.");
		tipVarLabels.setAutoHide(false);
				
		helpLabel.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseEnter(MouseEvent e) {	
				tipVarLabels.setVisible(true);
			}
			@Override
			public void mouseExit(MouseEvent e) {
				tipVarLabels.setVisible(false);
			}
		});	
		helpLabel.setImage(SWTResourceManager.getImage(ArcPropertiesDialog.class, Versys.ASSETS+"system-help-3.png"));				
		
		for(int i = PetriNet.TOKEN_NUM_TYPES; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
				TableItem item = new TableItem(tblVarWeights, SWT.NONE);
				try{ item.setText(0, Component.s_tokenIDs[i]); }
				catch(Exception e) {e.printStackTrace();}
				
				TableEditor editor = new TableEditor(tblVarWeights);
				m_spinners[i] = new Spinner(tblVarWeights, SWT.BORDER);
				m_spinners[i].setMaximum(9999);
				m_spinners[i].setSelection(m_arc.getArcWeights()[i]);
				editor.grabHorizontal = true;
				editor.setEditor(m_spinners[i], item, 1);		
		}		
		tblclmnVarFactor.pack();

		
		// Selection Listeners
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
			if(m_spinners[i] != null) {
				m_spinners[i].addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						int weightSum = 0;
						for(int k=0; k < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; k++) {
							if(m_spinners[k] != null) weightSum += m_spinners[k].getSelection();
						}
						if(weightSum == 0) ((Spinner)(e.getSource())).setSelection(1);
					}
				});
			}
		}
	}
}
