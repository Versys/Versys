/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Button;

import versys.petrinet.Component;
import versys.petrinet.PetriNet;
import versys.petrinet.Place;

import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

/**
 * What's a place without tokens? It's empty. So this is where you can fill it with colorful little dots.
 * @author Michael Rücker
 *
 */
public class PlacePropertiesDialog extends Dialog {

	private boolean result;
	private Shell shlPlatzEigenschaften;
	private Text textName;

	private final Place m_place;
	private final boolean[] m_universe;

	private final Spinner[] m_spinners;

	private Button btnLabelAnzeigen;
	private final Image[] m_images;

	/**
	 * Create the dialog.
	 * 
	 * @param parent the parent shell
	 * @param p the place, whose properties this is all about
	 */
	public PlacePropertiesDialog(Shell parent, Place p, boolean[] u) {
		super(parent, SWT.DIALOG_TRIM);
		getParent().setEnabled(false);

		m_place = p;
		m_universe = u.clone();
		m_spinners = new Spinner[PetriNet.TOKEN_NUM_TYPES];
		
		m_images = new Image[PetriNet.TOKEN_NUM_TYPES];
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			ImageData id = new ImageData(20, 20, 4, Component.s_paletteData);
			m_images[i] = new Image(Display.getDefault(), id);
			GC gc = new GC(m_images[i]);
			gc.setBackground(Component.s_tokenColors[i]);
			gc.fillRectangle(0, 0, 20, 20);
			gc.dispose();
		}
	}

	/**
	 * Open the dialog.
	 * 
	 * @return {@code true} if the place's properties were actually changed, {@code false} otherwise
	 */
	public boolean open() {
		createContents();
		shlPlatzEigenschaften.open();
		shlPlatzEigenschaften.layout();
		Display display = getParent().getDisplay();
		while (!shlPlatzEigenschaften.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			m_images[i].dispose();
		}
		
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlPlatzEigenschaften = new Shell(getParent(), getStyle());
		shlPlatzEigenschaften.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		shlPlatzEigenschaften.setSize(250, 330);
		shlPlatzEigenschaften.setText("Platz - Eigenschaften");

		Label lblBezeichnung = new Label(shlPlatzEigenschaften, SWT.NONE);
		lblBezeichnung.setBounds(10, 10, 87, 17);
		lblBezeichnung.setText("Beschriftung:");

		textName = new Text(shlPlatzEigenschaften, SWT.BORDER);
		textName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				textName.selectAll();
			}
		});
		textName.setBounds(10, 33, 224, 27);
		textName.setText(m_place.getLabel().getText());

		Label lblMarken = new Label(shlPlatzEigenschaften, SWT.NONE);
		lblMarken.setBounds(10, 85, 68, 17);
		lblMarken.setText("Marken:");

		btnLabelAnzeigen = new Button(shlPlatzEigenschaften, SWT.CHECK);
		btnLabelAnzeigen.setBounds(10, 234, 224, 21);
		btnLabelAnzeigen.setText("Bezeichnung anzeigen");
		btnLabelAnzeigen.setSelection(m_place.isLabelVisible());

		Button btnOk = new Button(shlPlatzEigenschaften, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				// Set Label
				String label = textName.getText();
				if (label.length() > 0) {
					m_place.getLabel().setText(label);
				}

				// Set tokens
				int[] tokens = new int[PetriNet.TOKEN_NUM_TYPES];
				for (int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
					if (m_spinners[i] != null)
						tokens[i] = m_spinners[i].getSelection();
				}
				m_place.setTokens(tokens);

				// Set drawLabel
				m_place.setLabelVisible(btnLabelAnzeigen.getSelection());

				result = true;
				shlPlatzEigenschaften.dispose();
			}
		});
		btnOk.setBounds(148, 261, 86, 27);
		btnOk.setText("OK");

		Button btnAbbrechen = new Button(shlPlatzEigenschaften, SWT.NONE);
		btnAbbrechen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				result = false;
				shlPlatzEigenschaften.dispose();
			}
		});
		btnAbbrechen.setBounds(10, 261, 86, 27);
		btnAbbrechen.setText("Abbrechen");

		Table table = new Table(shlPlatzEigenschaften, SWT.BORDER | SWT.V_SCROLL);
		table.setBounds(10, 108, 224, 120);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnFarbe = new TableColumn(table, SWT.NONE);
		tblclmnFarbe.setWidth(100);
		tblclmnFarbe.setText("Farbe");

		TableColumn tblclmnAnzahl = new TableColumn(table, SWT.NONE);
		tblclmnAnzahl.setWidth(100);
		tblclmnAnzahl.setText("Anzahl");

		for (int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			if (m_universe[i]) {
				TableItem item = new TableItem(table, SWT.NONE);
				
				item.setImage(0, m_images[i]);
				try{ item.setText(0, Component.s_tokenIDs[i]); }
				catch(Exception e) {e.printStackTrace();}
				

				TableEditor editor = new TableEditor(table);
				m_spinners[i] = new Spinner(table, SWT.BORDER);
				m_spinners[i].setMaximum(9999);
				m_spinners[i].setSelection(m_place.getTokens()[i]);
				editor.grabHorizontal = true;
				editor.setEditor(m_spinners[i], item, 1);
				
			}
		}
		// tblclmnFarbe.pack();
		tblclmnAnzahl.pack();
	}
}
