/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.dialogs;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Button;

import versys.Versys;
import versys.petrinet.Component;
import versys.petrinet.PetriNet;

import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * Dialog to change the net's current universe, i.e. the curently enabled token colors. Bacause life is much more than black and white...
 * @see PetriNet#getUniverse()
 * @see PetriNet#setUniverse(boolean[])
 * @author Michael Rücker
 *
 */
public class TokenColorDialog extends Dialog {
	
	private final Image[] m_images;
	private final boolean[] m_universe;
	private boolean[] m_result;
	private final Button[] m_chkButtons;
	
	private Shell shlMarkenfarben;

	/**
	 * Create the dialog.
	 * @param parent the dialog's parent shell
	 * @param universe the Petri net's universe, i.e. the set of available colors
	 */
	public TokenColorDialog(Shell parent, boolean[] universe) {
		super(parent, SWT.DIALOG_TRIM);
		setText("Markenfarben");
		getParent().setEnabled(false);
		m_universe = universe.clone();
		
		m_chkButtons = new Button[PetriNet.TOKEN_NUM_TYPES];
		
		m_images = new Image[PetriNet.TOKEN_NUM_TYPES];
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			ImageData id = new ImageData(20, 20, 4, Component.s_paletteData);
			m_images[i] = new Image(Display.getDefault(), id);
			GC gc = new GC(m_images[i]);
			gc.setBackground(Component.s_tokenColors[i]);
			gc.fillRectangle(0, 0, 20, 20);
			gc.dispose();
		}
	}

	/**
	 * Open the dialog.
	 * @return the resulting universe
	 */
	public boolean[] open() {
		createContents();
		shlMarkenfarben.open();
		shlMarkenfarben.layout();
		Display display = getParent().getDisplay();
		while (!shlMarkenfarben.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			m_images[i].dispose();
		}
		
		return m_result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		shlMarkenfarben = new Shell(getParent(), SWT.DIALOG_TRIM);
		shlMarkenfarben.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				getParent().setEnabled(true);
			}
		});
		shlMarkenfarben.setSize(249, 200);
		shlMarkenfarben.setText("Markenfarben");
		shlMarkenfarben.setLayout(null);

		Table table = new Table(shlMarkenfarben, SWT.BORDER | SWT.V_SCROLL);
		table.setBounds(10, 10, 223, 119);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnX = new TableColumn(table, SWT.CENTER);
		tblclmnX.setImage(SWTResourceManager.getImage(TokenColorDialog.class, Versys.ASSETS+"system-help-3.png")); // TODO: Add tooltip.
		tblclmnX.setWidth(50);


		TableColumn tblclmnFarbe = new TableColumn(table, SWT.NONE);
		tblclmnFarbe.setWidth(100);
		tblclmnFarbe.setText("Farbe");
		
		for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			TableItem item = new TableItem(table, SWT.BORDER | SWT.NO_FOCUS);
						
			item.setImage(1, m_images[i]);			
			try{ item.setText(1, Component.s_tokenIDs[i]); }
			catch(Exception e) {e.printStackTrace();}
			TableEditor editor = new TableEditor(table);
			m_chkButtons[i] = new Button(table, SWT.CHECK);
			m_chkButtons[i].setSelection(m_universe[i]);
			m_chkButtons[i].setBackground(SWTResourceManager.getColor(255, 255, 255));
			editor.grabHorizontal = true;
			editor.horizontalAlignment = SWT.CENTER;
			editor.setEditor(m_chkButtons[i], item, 0);					
		}		
		tblclmnX.pack();
		m_chkButtons[0].setEnabled(false);
		
		Button btnAbbrechen = new Button(shlMarkenfarben, SWT.NONE);
		btnAbbrechen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_result = null;
				shlMarkenfarben.dispose();
			}
		});
		btnAbbrechen.setBounds(10, 135, 86, 27);
		btnAbbrechen.setText("Abbrechen");
		
		Button btnOk = new Button(shlMarkenfarben, SWT.NONE);
		btnOk.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_result = new boolean[PetriNet.TOKEN_NUM_TYPES];
				for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
					if(m_chkButtons[i] != null)
						m_result[i] = m_chkButtons[i].getSelection();
				}
				shlMarkenfarben.dispose();
			}
		});
		btnOk.setBounds(147, 135, 86, 27);
		btnOk.setText("OK");

	}
}
