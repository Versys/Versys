/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys;

import org.eclipse.swt.widgets.Canvas;

import versys.dialogs.ErrorDialog;
import versys.petrinet.PNState;
import versys.petrinet.PetriNet;

/**
 * Class for handling an automatic simulation of a Petri net.
 * @author Michael Rücker
 *
 */
class AutoSimulation implements Runnable {

	private final Canvas m_canvas;
	private final RunLog m_runLog;
	private String m_tID;
	
	private final PetriNet m_petriNet;
	
	private int m_n;		// Current step.
	private final int m_num;		// The amount of simulated steps.
	private final int m_delay;	// Delay between each step in milliseconds.
	private final Versys m_versys;
	private final int m_id;
	
	/**
	 * Constructs a new AutoSimulation for the the given Petri net.
	 * @param canvas Canvas to draw on.
	 * @param pn Petri net for which to run the simulation
	 * @param num number of transition steps to be simulated
	 * @param delay delay between steps in milliseconds
	 * @param versys the {@link versys.Versys} instance that should display the simulations progress (usually the instance that contains the net's editor tab)
	 * @param id the editor tabs ID (needed to update the progress bar)
	 * @see org.eclipse.swt.widgets.Canvas
	 * @see org.eclipse.swt.widgets.ProgressBar
	 */
	public AutoSimulation(Canvas canvas, RunLog runLog, PetriNet pn, int num, int delay, Versys versys, int id) {
		m_canvas	= canvas;
		m_runLog 	= runLog;
		m_petriNet 	= pn;
		m_num 		= num;
		m_delay 	= delay;		
		m_versys	= versys;
		m_id 		= id;
		m_n			= 0;
	}
	
	/**
	 * Will keep firing random transitions of the Petri net until either the number of simulation steps has been reached or the automatic simulation is terminated externally.
	 * @see java.lang.Runnable#run()
	 */
	public void run() {			
				
		while(m_petriNet.getState() == PNState.PN_STATE_AUTO_SIM) {
			
			// Try firing a transition			
			m_tID = null;
			try {
				if(m_n < m_num) {
					
				//	synchronized (m_canvas) {
						m_tID = m_petriNet.fireRandomTransition();
				//	}
										
					if (m_tID != null) {
						m_n++;						
					}
				}							
			
			} catch(Exception e) {	
				e.printStackTrace();
				new ErrorDialog(m_canvas.getShell(), e.getMessage(), e);
			}
			
						
			// Redraw canvas														
			m_canvas.getDisplay().syncExec(new Runnable() {
                public void run() {
                	m_versys.updateProgressBar(m_id, m_n, m_num);
                	
    				if(m_tID != null && m_runLog != null)
                		m_runLog.log(m_tID);	
    				
                	m_canvas.redraw();
                }
			});					
						
			// Sleep
			try {
				Thread.sleep(m_delay);
			} catch (InterruptedException e) {e.printStackTrace();}
		}
	}

}
