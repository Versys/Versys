/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import versys.petrinet.Arc;
import versys.petrinet.Component;
import versys.petrinet.PetriNet;
import versys.petrinet.Place;
import versys.petrinet.Transition;

/**
 * Class responsible for parsing a Petri net from XML. Used for saving, loading and copying nets.
 * @author Michael Rücker
 *
 */
public class VersysFile {


	private PetriNet 	m_petriNet;
	private String		m_xml;
	
	private String		m_path;
	
	/**
	 * Constructs a new VersysFile.
	 * @param pn Petri net to be saved or copied
	 */
	public VersysFile(PetriNet pn) {
		m_petriNet 	= pn;
	}
	
	/**
	 * Constructs a new VersysFile.
	 * @param path path to an XML file which can then be parsed into a Petri net
	 */
	public VersysFile(String path) {
		m_path = path;
		
		try {
			
			BufferedReader br = new BufferedReader(
					new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8));
			
			m_xml = "";
			String line = br.readLine();			
			while(line != null) {
				m_xml += line;
				line = br.readLine();
			}
			br.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the XML representation of the Petri net
	 * @return XML representation of the Petri net
	 */
	private String getXML() {
		String xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		String xml = xmlHeader +"<pnml>\n";
		xml += m_petriNet.toXML();
		String xmlFooter = "</pnml>";
		xml += xmlFooter;
		return xml;
	//	return new String(xml.getBytes(), "UTF-8");
	}
	

	/**
	 * Parses an XML representation of a Petri net and adds the parsed components to the passed net.
	 * @param petriNet the parsed components will be added to this Petri net
	 * @param selectSubNet whether the parsed components should be selected
	 * @throws Exception If the XML is not a valid representation of a Petri net
	 */
	public void parseSubNet(PetriNet petriNet, boolean selectSubNet) throws Exception {
		
		if(m_xml == null) m_xml = getXML();		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = factory.newDocumentBuilder();
		
		InputStream is = new ByteArrayInputStream(m_xml.getBytes(StandardCharsets.UTF_8));
		Document doc = dBuilder.parse(is);		
		
		// Parse universe		
	//	boolean[] universe = new boolean[PetriNet.TOKEN_NUM_TYPES];
		boolean[] universe = petriNet.getUniverse();
		NodeList tokenTypes = doc.getElementsByTagName("token");
		for(int i=0; i<tokenTypes.getLength(); i++) {
			
			Element eTokenType = (Element) tokenTypes.item(i);
			
			// ID
			String ttID = eTokenType.getAttribute("id");//.getBytes("UTF-8");
			
			// Enabled			
			boolean ttEnabled = eTokenType.getAttribute("enabled").equals("true");
			
			for(int k=0; k < PetriNet.TOKEN_NUM_TYPES; k++) {		
				if(Component.s_tokenIDs[k].equals(ttID)) {
					universe[k] = (universe[k] || ttEnabled);
					break;
				}
			}
		}
		petriNet.setUniverse(universe);
		
		// Parse places
		NodeList places = doc.getElementsByTagName("place");
		for(int i=0; i<places.getLength(); i++) {
						
			Element ePlace = (Element) places.item(i);						
						
			// ID
			String pID = ePlace.getAttribute("id") + ".copy";	
			
			// Position
			Element ePos 	= (Element) ePlace.getElementsByTagName("graphics").item(0);
			Element eCoords	= (Element) ePos.getElementsByTagName("position").item(0);
			int pX = Integer.parseInt( eCoords.getAttribute("x") );
			int pY = Integer.parseInt( eCoords.getAttribute("y") );
			Place p = new Place(pX, pY, pID);
						
			// Label
			Element eName = (Element) ePlace.getElementsByTagName("name").item(0);			
			boolean visible = !eName.getAttribute("visible").equals("false");			
			Element eText = (Element) eName.getElementsByTagName("value").item(0);
			versys.petrinet.Label label = new versys.petrinet.Label(p);
			label.setText(eText.getTextContent());
			label.setVisible(visible);			
			
			Element eOffset = (Element) eName.getElementsByTagName("graphics").item(0);
			eCoords = (Element) eOffset.getElementsByTagName("offset").item(0);
			int lX = Integer.parseInt( eCoords.getAttribute("x") );
			int lY = Integer.parseInt( eCoords.getAttribute("y") );
			label.drag(pX+lX+5, pY+lY+10);			
						
			// Initial Marking
			Element eInitial = (Element) ePlace.getElementsByTagName("initialMarking").item(0);
			eText = (Element) eInitial.getElementsByTagName("value").item(0);
			String tokenString = eText.getTextContent();
			
			StringTokenizer st = new StringTokenizer(tokenString, ",");
			int[] iniTokens = new int[PetriNet.TOKEN_NUM_TYPES];
			int currTokenType = 0;
			while(st.hasMoreTokens()) {
				st.nextToken();	// Skip token name
				iniTokens[currTokenType] = Integer.parseInt(st.nextToken());
				currTokenType++;
			}
			p.setTokens(iniTokens);						
			
			petriNet.addComponent(p, selectSubNet);			
		}
		
		
		// Parse transitions
		NodeList transitions = doc.getElementsByTagName("transition");
		for(int i=0; i<transitions.getLength(); i++) {
			
			Element eTransition = (Element) transitions.item(i);			
			
			// ID
			String tID = eTransition.getAttribute("id")  + ".copy";
			
			// Position
			Element ePos 	= (Element) eTransition.getElementsByTagName("graphics").item(0);
			Element eCoords	= (Element) ePos.getElementsByTagName("position").item(0);
			int tX = Integer.parseInt( eCoords.getAttribute("x") );
			int tY = Integer.parseInt( eCoords.getAttribute("y") );
			Transition t = new Transition(tX, tY, tID);
			
			// Label
			Element eName = (Element) eTransition.getElementsByTagName("name").item(0);
			boolean visible = !eName.getAttribute("visible").equals("false");	
			Element eText = (Element) eName.getElementsByTagName("value").item(0);
			versys.petrinet.Label label = new versys.petrinet.Label(t);
			label.setText(eText.getTextContent());
			label.setVisible(visible);
			
			Element eOffset = (Element) eName.getElementsByTagName("graphics").item(0);
			eCoords = (Element) eOffset.getElementsByTagName("offset").item(0);
			int lX = Integer.parseInt( eCoords.getAttribute("x") );
			int lY = Integer.parseInt( eCoords.getAttribute("y") );
			label.drag(tX+lX+5, tY+lY+10);
			
			petriNet.addComponent(t, selectSubNet);
		}
		
		
		// Parse arcs
		NodeList arcs = doc.getElementsByTagName("arc");
		for(int i=0; i<arcs.getLength(); i++) {
			
			Element eArc = (Element) arcs.item(i);			
			
			// ID
			String aSource = eArc.getAttribute("source")  + ".copy";
			String aTarget = eArc.getAttribute("target")  + ".copy";			
			Arc a = petriNet.constructArc(aSource, aTarget);
			versys.petrinet.Label label = new versys.petrinet.Label(a);
			
			// Arc weights
			Element eInscription = (Element) eArc.getElementsByTagName("inscription").item(0);
			Element eText = (Element) eInscription.getElementsByTagName("value").item(0);
			String tokenString = eText.getTextContent();
			
			StringTokenizer st = new StringTokenizer(tokenString, ",");
			int[] arcWeights = new int[PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS];
			int currTokenType = 0;
			while(st.hasMoreTokens() && currTokenType < arcWeights.length) {
				st.nextToken();	// Skip token name
				arcWeights[currTokenType] = Integer.parseInt(st.nextToken());
				currTokenType++;
			}
			a.setArcWeights(arcWeights);
			
			Element eOffset = (Element) eInscription.getElementsByTagName("offset").item(0);
			int lX = Integer.parseInt( eOffset.getAttribute("x") );
			int lY = Integer.parseInt( eOffset.getAttribute("y") );			
			
			// Arc path			
			NodeList arcPath = eArc.getElementsByTagName("arcpath");
			if(arcPath.getLength() > 2) {
				
				Element eCurvePoint = (Element) arcPath.item(1);
				int cX = Integer.parseInt( eCurvePoint.getAttribute("x") );
				int cY = Integer.parseInt( eCurvePoint.getAttribute("y") );
				a.drag(cX, cY);
			}
			a.updatePosition();
			label.drag(lX+5, lY+10);			
			petriNet.addComponent(a, selectSubNet);
		}
				
		// Serialize Component IDs
		petriNet.serializeComponentIDs();		
	}
	
	/**
	 * Parses the XML representation of the Petri net into a new Petri net.
	 * @return a new PetriNet instance parsed from the XML
	 * @throws Exception If the XML is not a valid representation of a Petri net
	 */
	public PetriNet parsePetriNet() throws Exception {
		// TODO: What about escaped special characters?
		
		if(m_xml == null) m_xml = getXML();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = factory.newDocumentBuilder();
		
		InputStream is = new ByteArrayInputStream(m_xml.getBytes(StandardCharsets.UTF_8));
		Document doc = dBuilder.parse(is);		
		
		// Create Petri net		
		NodeList nets = doc.getElementsByTagName("net");		
		if(nets.getLength() > 1) throw new Exception("Datei enthält mehrere Netze");
		if(nets.getLength() < 1) throw new Exception("Datei enthält kein Petrinetz.");
		String netId = nets.item(0).getAttributes().getNamedItem("id").getNodeValue();
		PetriNet petriNet = new PetriNet(netId);
		
		parseSubNet(petriNet, false);
		
	//	System.out.println("done parsing net");
		return petriNet;
	}	
	

	/**
	 * Saves the XML representation of the Petri net to the given file.
	 * @param path path to file
	 */
	public void saveToXML(String path) {
		m_path = path;
		try {
			OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8);
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(getXML());
			bw.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}	
	
	/**
	 * Returns the file path the Petri net has been saved to, or null if the net has not been saved at least once.
	 * @return file path the Petri net has been saved to, or null if the net has not been saved.
	 */
	public String getFilePath() {		
		return m_path;		
	}
}
