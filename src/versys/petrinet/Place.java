/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Canvas;

import versys.dialogs.PlacePropertiesDialog;

/**
 * @author michael
 *
 */
public class Place extends Node {
	
//	public final static int Size = 50;
	
	private final static int[][] s_tokenDotPositions = {
		{-5, -5},	// first dot
		{-5, -20},	// second dot
		{-14, 8},	// ...
		{6, 8},
		{-20, -8},
		{10, -8}	// sixth dot
	};
	
	private final static int[][] s_tokenNumberPositions = {
		
		{-10, -8}, 	// If there is one token type
		
		{-10, -18}, 	// If there are two token types
		{-10,  2},
		
		{-10, - 22}, // If there are three token types
		{-10, -7},
		{-10,  8},
		
		{-10, -22}, // If there are four token types
		{-22, -7},
		{0, -7},
		{-10, 8},
		
		{-10, -26}, // If there are five token types
		{-24, -10},
		{2, -10},
		{-24, 6},
		{2, 6},
		
		{-22, -22}, // If there are six token types
		{2, -22},
		{-22, -7},
		{2, -7},
		{-22, 8},
		{2, 8},
		
		{-24, -26}, // If there are seven token types
		{2, -26},
		{-34, -7},
		{-10, -7},
		{14, -7},
		{-24, 12},
		{2, 12},
		
		{-24, -30}, // If there are eight token types
		{2, -30},
		{-34, -7},
		{-10, -15},
		{-10, 2},
		{14, -7},
		{-24, 16},
		{2, 16},
				
	};
	
	private final static int[] s_positionIndexes = {0,1,3,6,10,15,21,28};
		
	private final int[] m_iniTokens;
	private final int[] m_currTokens;
	
	
	public Place(int x, int y) {
		this(x, y, null);		
	}
	
	public Place(int x, int y, String id) {
		super();
		m_type 		= CType.CTYPE_PLACE;
		m_id		= id;
		
		m_x			= x;
		m_y			= y;
		m_size		= 50;
		
		m_iniTokens		= new int[PetriNet.TOKEN_NUM_TYPES];
		m_currTokens 	= new int[PetriNet.TOKEN_NUM_TYPES];
	}
	
	public void draw(GC gc) {		
			
		// Get token amount and types
		int tokenTypes = 0;			
		int tokenAmount = 0;
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			if(m_currTokens[i] > 0) {
				tokenTypes++;
				tokenAmount += m_currTokens[i];
			}
			
		}
		
		// Determine size of place
		if(tokenAmount < 7 || tokenTypes < 5) m_size = 50;
		else if (tokenTypes < 7) m_size = 65;
		else m_size = 75;
		
		// Start drawing
		gc.setLineWidth(2);
		
		// Draw place
		gc.setBackground(s_componentColors[0]);
		if(m_isSelected) 	gc.setForeground(s_componentColors[2]);
		else				gc.setForeground(s_componentColors[1]);
		gc.fillOval(m_x-(m_size/2), m_y-(m_size/2), m_size, m_size);
		gc.drawOval(m_x-(m_size/2), m_y-(m_size/2), m_size, m_size);	
		
		
		// Draw tokens as dots
		if(tokenAmount < 7) {
			
			int currToken = 0;
			
			for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
				gc.setBackground(s_tokenColors[i]);
				for(int j = 0; j < m_currTokens[i]; j++) {
					gc.fillOval(m_x+s_tokenDotPositions[currToken][0], m_y+s_tokenDotPositions[currToken][1], 10, 10);
					currToken++;
				}
			}
			
		// Draw tokens as numbers
		} else {			
			
			int posIndex = s_positionIndexes[tokenTypes-1];					
			
			for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
				
				if(m_currTokens[i] > 0) {
					gc.setForeground(s_tokenColors[i]);
					gc.drawString(String.valueOf(m_currTokens[i]), 
							m_x+s_tokenNumberPositions[posIndex][0], 
							m_y+s_tokenNumberPositions[posIndex][1],
							true);
					posIndex++;
				}
			}
		}
		
		// Draw label
		m_label.draw(gc);
	}
	
	public void drag(int x, int y) {
		m_x = x;
		m_y = y;
	}	
	
	public void addToken(int num, int type) {
		m_currTokens[type] += num;
	}
	
	public void removeToken(int num, int type) {
		if(m_currTokens[type] < num) return;
		m_currTokens[type] -= num;
	}
	
	public void setTokens(int[] tokens) {
		if (PetriNet.TOKEN_NUM_TYPES >= 0)
			System.arraycopy(tokens, 0, m_currTokens, 0, PetriNet.TOKEN_NUM_TYPES);
	}

	public boolean checkSelect(int x, int y) {
		
		int dx = x-m_x;
		int dy = y-m_y;
        return (dx * dx + dy * dy) <= (m_size * m_size / 4);
	}

	public void startSimulation() {
		super.startSimulation();
		if (PetriNet.TOKEN_NUM_TYPES >= 0)
			System.arraycopy(m_currTokens, 0, m_iniTokens, 0, PetriNet.TOKEN_NUM_TYPES);
	}
	
	public void stopSimulation() {
		super.stopSimulation();
		if (PetriNet.TOKEN_NUM_TYPES >= 0)
			System.arraycopy(m_iniTokens, 0, m_currTokens, 0, PetriNet.TOKEN_NUM_TYPES);
	}
	
	public int[] getTokens() {
		return m_currTokens;
	}
	
	public String toXML() {
		
		// Place id
		String xml = "<place id=\"" + m_id + "\">\n";
		
		// Position
		xml += "<graphics>\n" +
				"<position x=\"" + m_x +"\" y =\"" + m_y + "\"/>\n" +
				"</graphics>\n";
		
		// Label
		xml += m_label.toXML();
		
		// Initial marking
		xml += "<initialMarking>\n<value>";
		for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			if(m_isSimulationRunning)
				xml += Component.s_tokenIDs[i] + "," + m_iniTokens[i]+",";
			else
				xml += Component.s_tokenIDs[i] + "," + m_currTokens[i]+",";
		}
			 		
		xml = xml.substring(0, xml.length()-1);
		xml += "</value>\n</initialMarking>\n";
		
		xml += "</place>\n";
		
		return xml;
	}
	
	public boolean openPropertiesDialog(Canvas canvas) {
		
		PlacePropertiesDialog dia = new PlacePropertiesDialog(canvas.getShell(), this, m_petrinet.getUniverse());
		return dia.open();			
	}

	public void removeTokenType(int type) {
		m_currTokens[type] = 0;
	}
	
	public Node copy() {
		Place p = new Place(m_x, m_y, m_id);
		p.setTokens(getTokens());
		
		Label l = new Label(p);
		l.drag(m_label.getX()+5, m_label.getY()+10);
		l.setText(m_label.getText());
		
		return p;
	}
}
