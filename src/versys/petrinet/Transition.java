/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.wb.swt.SWTResourceManager;

import versys.dialogs.TransitionPropertiesDialog;

/**
 * @author michael
 *
 */
public class Transition extends Node {
	
//	public final static int Size = 20;
	
	private final Color m_colorEnabled;
	
	private ArrayList<ArrayList<int[]>> m_arcModes;
	private ArrayList<int[]> m_possibleModes;
	
	public Transition(int x, int y) {
		this(x, y, null);
	}
	
	public Transition(int x, int y, String id) {
		super();
		m_type = CType.CTYPE_TRANS;
		m_id = id;
		
		m_x 	= x;
		m_y 	= y;
		m_size 	= 20;
		
		m_colorEnabled = SWTResourceManager.getColor(50,180,50);		
	}
	
	public void draw(GC gc) {
		if(m_isSimulationRunning && isEnabled())			gc.setBackground(m_colorEnabled);
		else if(!m_isSimulationRunning && m_isSelected)		gc.setBackground(s_componentColors[2]);		
		else												gc.setBackground(s_componentColors[1]);
		
		gc.fillRectangle(m_x-(m_size/2), m_y-(m_size/2), m_size, m_size);
		
		// Draw label
		m_label.draw(gc);
	}
	
	public void drag(int x, int y) {
		m_x = x;
		m_y = y;
	}
	
	
	public boolean checkSelect(int x, int y) {		
		if(x < m_x-(m_size/2)) 	return false;
		if(x > m_x+(m_size/2)) 	return false;
		if(y < m_y-(m_size/2))	return false;
		return y <= m_y + (m_size / 2);
	}
	
	public synchronized boolean isEnabled() { 
		
		// Reset the mode sets
		m_arcModes 		= new ArrayList<>();
		m_possibleModes = new ArrayList<>();
		
		// There are no incoming arcs, so we are done
		if(m_inArcs.isEmpty()) return true;
		
		// See if all incoming arcs are fulfilled.
		// If an arc has variable arc labelings, add its mode set to the list.
		for (Arc a : m_inArcs) {
			ArrayList<int[]> arcModes = a.isFulfilled();
			if (arcModes == null) {
				return false;
			}
			if (a.hasVariableArcLabelings()) {
				m_arcModes.add(arcModes);
			}
		}
		
		// there are no mode sets, so we are done
		if(m_arcModes.isEmpty()) {
			return true;
		
		//there is at least one mode set, get their cut
		} else {
			// get cut of all mode sets
			if(m_arcModes.size() > 1) {				
				for(int i=0; i < m_arcModes.get(0).size(); i++) {
					getPossibleModes(0, i, null);
				}
				return !m_possibleModes.isEmpty();
			
			// There is only one mode set, so use that 
			} else {
				m_possibleModes = m_arcModes.get(0);
				return true;
			}
			
		}
	}

	private void getPossibleModes(int i, int j, int[] curMode) {
				
		if(i == 0) {
			curMode = m_arcModes.get(i).get(j).clone();
		
		} else {				
			int[] mode = m_arcModes.get(i).get(j);

			for(int k=0; k < PetriNet.NUM_VARS; k++) {
				
				if(curMode[k] < 0)	{
					curMode[k] = mode[k];
				} else {
					if(mode[k] > -1 && curMode[k] != mode[k]) {
						return;
					}
				}
									
			}
			
		}
		
		if(i == m_arcModes.size()-1) {
			m_possibleModes.add(curMode.clone());
		
		} else {
			for(int k=0; k < m_arcModes.get(i+1).size(); k++) {
				getPossibleModes(i+1, k, curMode.clone());
			}
		}
	}

	public synchronized void fire() throws Exception {
		
		int[] mode = null;
		
		// Incoming arcs have variable arc labelings
		if(!m_possibleModes.isEmpty()) {
												
			// Randomly select one of their common possible modes
			mode = m_possibleModes.get( (int)(Math.random()*m_possibleModes.size()) ).clone();
			
			// If there are unbound variables, randomly bind them:
			for(int i=0; i < mode.length; i++) {
				int binding;
				do {
					binding = (int)(Math.random()*PetriNet.TOKEN_NUM_TYPES);
				} while(!m_petrinet.getUniverse()[binding]);
				
				if(mode[i] < 0) mode[i] = binding;
			}
			
			// DEBUG: print possible modes and selected mode
			/*
			System.out.println("Possible Modes:");
			Iterator<int[]> itModes = m_possibleModes.iterator();
			while(itModes.hasNext()) {
				int[] dMode = itModes.next();
				for (int value : dMode) {
					System.out.print(" " + value);
				}
				System.out.println();
			}
			
			System.out.println("Selected mode:");
			for (int value : mode) {
				System.out.print(" " + value);
			}
			System.out.println(); 
			//*/
		
		// Incoming arcs have constant arc labelings
		} else {
			
			// Check if at least one outgoing arc has a variable arc labeling
			boolean outVars = false;
			for (Arc m_outArc : m_outArcs) {
				if (m_outArc.hasVariableArcLabelings()) {
					outVars = true;
					break;
				}
			}
			
			// If so, randomly generate a mode
			if(outVars) {
				mode = new int[PetriNet.TOKEN_NUM_TYPES];
								
				ArrayList<Integer> bindings = new ArrayList<>();
				for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++)
					if(m_petrinet.getUniverse()[i]){
						bindings.add(i);						
					}
				
											
				for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++)					
					mode[i] = bindings.get( (int) (Math.random()*bindings.size()) ); 					
			}			
		}
		
		Iterator<Arc> itArc = m_inArcs.iterator();
		while(itArc.hasNext()) itArc.next().fire(mode);		
		
		itArc = m_outArcs.iterator();
		while(itArc.hasNext()) itArc.next().fire(mode);
	}

	public String toXML() {
		
		// Id
		String xml = "<transition id=\"" + m_id + "\">\n";
		
		// Position
		xml += "<graphics>\n" +
				"<position x=\"" + m_x + "\" y=\"" + m_y + "\"/>\n" +
				"</graphics>\n";
		
		// Label
		xml += m_label.toXML();
		
		xml += "</transition>\n";
		
		return xml;
	}
	
	public boolean openPropertiesDialog(Canvas canvas) {
		
		TransitionPropertiesDialog dia = new TransitionPropertiesDialog(canvas.getShell(), this);		
		return dia.open();
	}

	public void removeTokenType(int type) {}
	
	public int getSize() {
		return m_size + 2;
	}
	
	/**
	 * Returns a copy of the transition, WITHOUT its incoming and outgoing arcs.
	 */
	public Node copy() {
		Transition t = new Transition(m_x, m_y, m_id);
		
		Label l = new Label(t);
		l.drag(m_label.getX()+5, m_label.getY()+10);
		l.setText(m_label.getText());
		
		return t;
	}
}
