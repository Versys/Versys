/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.wb.swt.SWTResourceManager;

/**
 * Abstract superclass of all Petri net components, i.e., places, transitions, arcs and labels.
 * @author Michael Rücker
 */
abstract public class Component {
	
	final static Color[] 	s_componentColors = {
		SWTResourceManager.getColor(255, 255, 255),
		SWTResourceManager.getColor(0, 0, 0),
		SWTResourceManager.getColor(150, 200, 255),
	};
	
	public final static Color[] 	s_tokenColors = {
		SWTResourceManager.getColor(0, 0, 0),
		SWTResourceManager.getColor(255, 0, 0),
		SWTResourceManager.getColor(0, 200, 0),
		SWTResourceManager.getColor(80, 80, 255),
		SWTResourceManager.getColor(255, 150, 0),
		SWTResourceManager.getColor(110, 75, 0),
		SWTResourceManager.getColor(140, 50, 255),
		SWTResourceManager.getColor(120, 240, 240)
	};
	
	public final static PaletteData s_paletteData = new PaletteData(new RGB[] {			
		s_tokenColors[0].getRGB(),
		s_tokenColors[1].getRGB(),
		s_tokenColors[2].getRGB(),
		s_tokenColors[3].getRGB(),
		s_tokenColors[4].getRGB(),
		s_tokenColors[5].getRGB(),
		s_tokenColors[6].getRGB(),
		s_tokenColors[7].getRGB()
	});
	
	/**
	 * The token IDs as Strings (Schwarz, Rot, Grün, Blau, ...).
	 */
	public static final String[] s_tokenIDs = {
		"Schwarz",
		"Rot",
		"Grün",
		"Blau",
		"Orange",
		"Braun",
		"Violett",
		"Cyan",
		
		"X",
		"Y",
		"A",
		"B",
		
		"U",
		"V",
		"S",
		"T",
	};
	
	int			m_x;
	int			m_y;
	
	boolean 	m_isSelected;
	
	boolean		m_isDeleted;
	
	CType		m_type;
	String		m_id;
	
	PetriNet 	m_petrinet;
	
	boolean		m_isSimulationRunning;
	
	Label		m_label;
	
	/**
	 * Constructs a new component.
	 */
	Component() {
		m_isDeleted		= false;
		m_isSimulationRunning = false;		
	}	
	
	/**
	 * Returns the components type, i.e., whether it is a place, transition, arc or label.
	 * @return the component's type
	 */
	public CType getType() {
		return m_type;
	}
	
	/**
	 * Returns the component's x-coordinate.
	 * @return the component's x-coordinate
	 */
	public int getX() {
		return m_x;
	}
	
	/**
	 * Returns the component's y-coordinate.
	 * @return the component's y-coordinate.
	 */
	public int getY() {
		return m_y;
	}
	
	/**
	 * Marks the component as selected, causing it to render accordingly.
	 */
	public void select() {
		m_isSelected = true;
	}
	
	/**
	 * Returns whether the component is currently marked as selected.
	 * @return {@code true} if the component is currently selected, {@code false} otherwise
	 */
	public boolean isSelected() {
		return m_isSelected;
	}
	
	/**
	 * Marks the component as not selected, causing to render accordingly.
	 */
	public void unselect() {
		m_isSelected = false;
	}
	
	/**
	 * Puts the component in simulation mode.
	 */
	public void startSimulation() {
		m_isSimulationRunning = true;
	}
	
	/**
	 * Puts the component in edit mode.
	 */
	public void stopSimulation() {
		m_isSimulationRunning = false;
	}

	/**
	 * Sets the component's label.
	 * @param l the new label
	 */
	void setLabel(Label l) {
		m_label = l;
	}
	
	/**
	 * Returns the component's label.
	 * @return the component's label
	 */
	public Label getLabel() {
		return m_label;
	}
	
	/**
	 * Returns whether the component has been deleted from the net.
	 * @return {@code true} if the component has been deleted, {@code false} otherwise
	 */
	public boolean isDeleted() {
		return m_isDeleted;
	}
	
	/**
	 * Returns the component's ID.
	 * @return the component's ID
	 */
	public String getID() {
		return m_id;
	}
	
	/**
	 * Sets the component's ID to the specified String
	 * @param id the component's new ID
	 */
	public void setID(String id) {
		m_id = id;
	}
	
	
	public void registerPetriNet(PetriNet net) {
		m_petrinet = net;
	}
	
	// === Abstract methods ===		
	
	/**
	 * Returns the XML-representation of the component.
	 * @return the XML-representation of the component
	 */
	public abstract String toXML();	
	
	/**
	 * Draws the component onto the specified canvas.
	 * @param gc the Canvas the component will be drawn on
	 */
	public abstract void draw(GC gc);
	
	/**
	 * Drags (i.e., moves) the component to the specified coordinates.
	 * @param x the drag point's x-coordinate
	 * @param y the drag point's y-coordinate
	 */
	public abstract void drag(int x, int y);
	
	/**
	 * Returns whether the component covers the specified coordinates, that is, whether a click on the
	 * specified point would select the component.
	 * @param x the point's x-coordinate
	 * @param y the point's y-coordinate
	 * @return {@code true} if the component covers the specified coordinates, {@code false} otherwise
	 */
	public abstract boolean checkSelect(int x, int y);
	
	/**
	 * Returns whether the component's coordinates lie within the specified rectangle, that is, whether a corresponding
	 * selection rectangle would select the component.
	 * @param r the selection rectangle
	 * @return {@code true} if the component's coordinates lie within the specified rectangle, {@code false} otherwise
	 */
	public abstract boolean checkSelect(Rectangle r);
	
	/**
	 * Opens the component's properties dialog. 
	 * @param canvas the canvas which will be the dialog's parent
	 * @return {@code true} if the component was changed, {@code false} otherwise
	 */
	public abstract boolean openPropertiesDialog(Canvas canvas);
	
	
	public abstract void removeTokenType(int type);
	
	/**
	 * Marks the component as deleted and removes it from the specified Petri net.
	 * @param pn the Petri net the component will be removed from
	 */
	public abstract void delete(PetriNet pn);
}
