/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.graphics.PathData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;

import versys.dialogs.ArcPropertiesDialog;

/**
 * @author michael
 *
 */
public class Arc extends Component {
		
	private final static double s_cosa1 = Math.cos(.4); 
	private final static double s_sina1 = Math.sin(.4);
	private final static double s_cosa2 = Math.cos(-.4);
	private final static double s_sina2 = Math.sin(-.4);
	
	private final Node 	m_source; 	// Arc is going out of this Node.
	private final Node 	m_target;	// Arc is going into this Node.
	
	private final PathData	m_pathData;
	private int		m_numPoints;
	
	private final int[]		m_arcWeights;
	private int		m_numVars;
	private int[]		m_vars;	
	
	public Arc(Node source, Node target) {
		super();
		m_type 	= CType.CTYPE_ARC;
		
		m_source 		= source;
		m_target 		= target;		
		m_source.addOutArc(this);
		m_target.addInArc(this);
		m_id = m_source.getID() + " to " + m_target.getID();
		
		m_numPoints = 2;		
		m_pathData = new PathData();
		m_pathData.points = new float[2*m_numPoints];
		m_pathData.types = new byte[m_numPoints];
		m_pathData.types[0] = SWT.PATH_MOVE_TO;
		m_pathData.types[1] = SWT.PATH_LINE_TO;
		
		m_arcWeights = new int[PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS];
		m_arcWeights[PetriNet.TOKEN_BLACK] = 1;		
		m_numVars = 0;
		
		updatePosition();
	}
	
	public void draw(GC gc) {
				
		int x1 = m_source.getX();
		int y1 = m_source.getY();
		int x2 = m_target.getX();
		int y2 = m_target.getY();
		
		// See which direction arc is going
		float cut1, cut2;
		cut1 = m_source.getSize() / 2.0f;
		cut2 = m_target.getSize() / 2.0f;	
				
		// Arc is a straight line
		if(m_numPoints == 2) {
				
			int[] arc = getEndpoints(x1, y1, x2, y2, cut1, cut2);
			for(int i=0; i<m_numPoints*2; i++) 
				m_pathData.points[i] = arc[i];

		// Arc is a cubic curve
		} else {
			
			int[] arcSec1 = getEndpoints(x1, y1, m_pathData.points[2], m_pathData.points[3], cut1, 0.0f);
			int[] arcSec2 = getEndpoints(m_pathData.points[4], m_pathData.points[5], x2, y2, 0.0f, cut2);
			
			m_pathData.points[0] = arcSec1[0];
			m_pathData.points[1] = arcSec1[1];
			m_pathData.points[6] = arcSec2[2];
			m_pathData.points[7] = arcSec2[3];
		}
		

		// Create path from path data		
		Path path = new Path(gc.getDevice(), m_pathData);
		
		// Draw path
		gc.setLineWidth(2);
		if(isSelected())	gc.setForeground(s_componentColors[2]);
		else				gc.setForeground(s_componentColors[1]);
		gc.drawPath(path);

		// Calculate arrow head
		int[] arcHead = getArcHead(
				m_pathData.points[2*m_numPoints-4],
				m_pathData.points[2*m_numPoints-3],
				m_pathData.points[2*m_numPoints-2],
				m_pathData.points[2*m_numPoints-1]);

		// Draw arrow head
		if(isSelected())	gc.setBackground(s_componentColors[2]);
		else				gc.setBackground(s_componentColors[1]);
		gc.fillPolygon(arcHead);
	
		// Clean up
		path.dispose();
		
		updatePosition();
		m_label.draw(gc);
	}
	
	public void drag(int x, int y) {		
		if(m_numPoints == 2) {
			m_numPoints = 4;
			
			float[] newPoints = new float[2*m_numPoints];
			newPoints[0] = m_pathData.points[0];
			newPoints[1] = m_pathData.points[1];			
			newPoints[6] = m_pathData.points[2];
			newPoints[7] = m_pathData.points[3];
			m_pathData.points = newPoints;
			
			m_pathData.types[1] = SWT.PATH_CUBIC_TO;			
		}
		
		m_pathData.points[2] = x; 
		m_pathData.points[3] = y; 
		m_pathData.points[4] = x; 
		m_pathData.points[5] = y; 
		
		updatePosition();
	}
	
	public boolean isCurved() {
		return m_numPoints > 2;
	}

	public void resetCurve() {
		if(!isCurved()) return;
		
		m_numPoints = 2;
		
		float[] newPoints = new float[2*m_numPoints];
		newPoints[0] = m_pathData.points[0];
		newPoints[1] = m_pathData.points[1];
		newPoints[2] = m_pathData.points[6];
		newPoints[3] = m_pathData.points[7];
		m_pathData.points = newPoints;
		m_pathData.types[1] = SWT.PATH_LINE_TO;
	}
	
	public boolean checkSelect(int x, int y) {
		
		// Arc is a line, so we measure the point's distance from it.
		if(m_numPoints == 2) {

			// Arc vector
			double v1x = m_target.getX() - m_source.getX();
			double v1y = m_target.getY() - m_source.getY();
			
			// Click-Vector
			double v2x = x - m_source.getX();
			double v2y = y - m_source.getY();
			
			// Normalize arc vector
			double v1m = Math.sqrt(v1x*v1x + v1y*v1y); 
			v1x /= v1m;
			v1y /= v1m;
			
			// Vector projection			
			double projection = (v1x*v2x + v1y*v2y);
			if(projection < 0 || projection > v1m) return false; 			
			
			// Scale arc vector
			v1x *= projection;
			v1y *= projection;
			
			// Distance vector
			double dx = v1x - v2x;
			double dy = v1y - v2y;
			double dist = Math.sqrt(dx*dx + dy*dy);

			return dist < 3.0;
			
			
		// Arc is a curve, so we use a dirty hack with Path.contains(x,y).
		} else {
			
			Path path = new Path(Display.getDefault(), m_pathData);
			GC gc = new GC(Display.getDefault());
			
			boolean selected = false;
			
			for (float ix = x-3.0f; ix <= x+3.0f && !selected; ix += 0.1f) {
				for (float iy = y-3.0f; iy <= y+3.0f && !selected; iy += 0.1f) {
					selected = path.contains(ix, iy, gc, true); 
				}
			}
			
			path.dispose();
			gc.dispose();		
			
			return selected;
		}
	}
	
	public boolean checkSelect(Rectangle r) {
		if(m_numPoints == 2) 
			return m_source.isSelected() && m_target.isSelected();
		
		return (m_source.isSelected() || m_target.isSelected()) && 
				r.contains((int)m_pathData.points[2], (int)m_pathData.points[3]);
	}
	
	public boolean hasVariableArcLabelings() {
		return (m_numVars > 0);
	}

	public ArrayList<int[]> isFulfilled() {

		if(m_source.getType() != CType.CTYPE_PLACE) return null; 
		
		int[] sourceTokens = ((Place)m_source).getTokens().clone();		
		
		// constant arc labelings
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			sourceTokens[i] -= m_arcWeights[i];			
			if(sourceTokens[i] < 0) return null;
		}
				
		// variable arc labelings
		if(!hasVariableArcLabelings()) {
			return new ArrayList<>();
			
		} else {			
			return getArcModes(sourceTokens);
		}				
	}
	
	
	private ArrayList<int[]> getArcModes(final int[] sourceTokens) {
				
		// Initialize mode set
		String fullSet = "";
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
			if(m_petrinet.getUniverse()[i])
				fullSet += String.valueOf(i);
		}
				
		String[] possibleBindings = new String[m_numVars];
		for(int i=0; i < m_numVars; i++) {
			possibleBindings[i] = fullSet;			
		}
		

		// Eliminate bindings where the variable factor is higher than the amount of tokens in the place,
		for(int i=0; i < m_numVars; i++) {			
			
			for(int j=0; j < PetriNet.TOKEN_NUM_TYPES; j++) {
				
				if(m_arcWeights[ m_vars[i] ] > sourceTokens[j]) {
					String s = Character.toString((char)(j+48));
					possibleBindings[i] = possibleBindings[i].replace(s, "");
					if(possibleBindings[i].length() < 1) return null;					
				}
			}			
		}
			
		// If at least one variable has only one possible binding, we can limit the mode set further
		boolean changed;
		do {
			changed = false;
			for(int i=0; i<m_numVars; i++) {
				
				// See if variable has only one possible binding
				if(possibleBindings[i].length() == 1) {
					
					// If so, see if another variable could still be bound to the same token type
					int ident = Integer.parseInt(possibleBindings[i]);
					int remains = sourceTokens[ident] - m_arcWeights[ m_vars[i] ];
					for(int j=0; j < m_numVars; j++) {
						if(j == i) continue;
						
						// If not enough tokens remain, delete binding
						if(possibleBindings[j].contains(possibleBindings[i]) && m_arcWeights[ m_vars[j] ] > remains) {
							possibleBindings[j] = possibleBindings[j].replace(possibleBindings[i], "");
							if(possibleBindings[j].length() < 1) return null;							
							changed = true;
						}
					}					
				}				
			}			
		} while (changed);
		
		// Recursively construct mode set
		ArrayList<int[]> modeList = new ArrayList<>();
		for(int i=0; i < possibleBindings[0].length(); i++) {
			int[] mode = new int[PetriNet.NUM_VARS];
			for(int j=0; j<mode.length; j++) mode[j] = -1;
			getVarBindings(possibleBindings, modeList, 0, i, mode, sourceTokens);
		}
		
		// DEBUG
		/*
		System.out.println("ArrayList:");
		Iterator<int[]> it = identList.iterator();
		while(it.hasNext()) {
			System.out.print(" ");
			int[] curIdents = it.next();			
			for(int i=0; i<curIdents.length; i++)
				System.out.print(curIdents[i]+" ");
			System.out.println();
		} //*/
		
		if(modeList.isEmpty())	return null;
		else					return modeList;
	}
	
	/**
	 * It's recursive. Here be dragons!
	 * @see #getVarBindings(String[], ArrayList, int, int, int[], int[])
	 */
	private void getVarBindings(String[] possibleBinds, ArrayList<int[]> modeList, int curVar, int curBind, int[] curMode, final int[] sourceTokens) {
		
		int bind = Integer.parseInt(possibleBinds[curVar].substring(curBind, curBind+1));
		int remains = sourceTokens[bind];
		for(int i=0; i < curVar; i++) {
			if(curMode[m_vars[i]-PetriNet.TOKEN_NUM_TYPES] == bind) {
				remains -= m_arcWeights[ m_vars[i] ];
				if(remains < m_arcWeights[ m_vars[curVar] ]) return;
			}
		}
		curMode[ m_vars[curVar]-PetriNet.TOKEN_NUM_TYPES ] = bind; 
		
		if(curVar == m_numVars-1) {
			modeList.add(curMode.clone());
			
		} else {
			for(int k=0; k < possibleBinds[curVar+1].length(); k++) {
				getVarBindings(possibleBinds, modeList, curVar+1, k, curMode, sourceTokens);
			}
		}
	}
	
	public void fire(int[] mode) throws Exception {
		
		if(mode == null && hasVariableArcLabelings())
			throw new Exception("Arc '"+getID()+"' has variable arc labelings, but mode is null.");
		
		if(m_source.getType() == CType.CTYPE_PLACE) {
			for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
				((Place)m_source).removeToken(m_arcWeights[i], i);
			}				
			
			if(mode != null) {
				for(int i=0; i < PetriNet.NUM_VARS; i++) {
					((Place)m_source).removeToken(m_arcWeights[i+PetriNet.TOKEN_NUM_TYPES], mode[i]);
				}
			}
				
		
		} else {
			for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
				((Place)m_target).addToken(m_arcWeights[i], i);				
			}
			
			if(mode != null) {
				for(int i=0; i < PetriNet.NUM_VARS; i++) {
					((Place)m_target).addToken(m_arcWeights[i+PetriNet.TOKEN_NUM_TYPES], mode[i]);
				}
			}
				
		}			
	}
	
	
	public int[] getArcWeights() {
		return m_arcWeights;
	}
	
	
	public void setArcWeights(int[] weights) {
        if (PetriNet.TOKEN_NUM_TYPES + PetriNet.NUM_VARS >= 0)
            System.arraycopy(weights, 0, m_arcWeights, 0, PetriNet.TOKEN_NUM_TYPES + PetriNet.NUM_VARS);
		
		m_numVars = 0;
		for(int i = PetriNet.TOKEN_NUM_TYPES; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
			if(m_arcWeights[i] > 0) m_numVars++;
		}
		
		m_vars = new int[m_numVars];
		
		int curVar = 0;
		for(int i = PetriNet.TOKEN_NUM_TYPES; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
			if(m_arcWeights[i] > 0) m_vars[curVar++] = i;
		}
	}
	
	
	private Component getSource() {
		return m_source;
	}
	
	
	private Component getTarget() {
		return m_target;
	}
	
	
	public boolean equals(Arc a) {
		return (a.getSource() == m_source && a.getTarget() == m_target);
	}
	
	
	private int[] getEndpoints(float x1, float y1, float x2, float y2, float cut1, float cut2) {
		
		int[] endPoint = new int[4];
				
		float dx = x1 - x2;
		float dy = y1 - y2;
		
		double dist = Math.sqrt(dx*dx + dy*dy);
		
		double nx = dx / dist;
		double ny = dy / dist;
		
		endPoint[0] = (int)(x1 - nx*cut1);
		endPoint[1] = (int)(y1 - ny*cut1);
		endPoint[2] = (int)(x2 + nx*cut2);
		endPoint[3] = (int)(y2 + ny*cut2);

		return endPoint;
	}
	
	
	private int[] getArcHead(float x1, float y1, float x2, float y2) {
		
		float dx = x1 - x2;
		float dy = y1 - y2;
		
		double dist = Math.sqrt(dx*dx + dy*dy);
		
		float nx = (float)(dx / dist) * 20;
		float ny = (float)(dy / dist) * 20;
			
		double c1x = s_cosa1 * nx - s_sina1 * ny + x2;
		double c1y = s_sina1 * nx + s_cosa1 * ny + y2;

		double c2x = s_cosa2 * nx - s_sina2 * ny + x2;
		double c2y = s_sina2 * nx + s_cosa2 * ny + y2;

		return new int[]{(int)x2,(int)y2,(int)c1x,(int)c1y,(int)c2x,(int)c2y};
	}
	
	
	public void updatePosition() {
		
		if(m_numPoints == 2) {
			int dx = m_source.getX() - m_target.getX();
			int dy = m_source.getY() - m_target.getY();		
			m_x = m_source.getX() - (dx / 2);
			m_y = m_source.getY() - (dy / 2);
		
		} else {
			m_x = (int)m_pathData.points[2];
			m_y = (int)m_pathData.points[3];
		}
	}
	
	
	public void updateID() {
		m_id = m_source.getID() + " to " + m_target.getID();
	}
	
	
	public String toXML() {
		
		// Id
		String xml = "<arc id=\"" + m_id + "\" source=\"" + m_source.getID() + "\" target=\"" + m_target.getID() + "\">\n";
		
		// Arc weights
		xml += "<inscription>\n<value>";
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++)
			xml += Component.s_tokenIDs[i] + "," + m_arcWeights[i]+",";
		xml = xml.substring(0, xml.length()-1);		
		xml += "</value>\n" +
				"<offset x=\""+m_label.getX()+"\" y=\""+m_label.getY()+"\"/>" +
				"</inscription>\n";
			
		// Arc path
		xml += "<arcpath id=\"0\" x=\"" + (int)m_pathData.points[0] + "\" y=\"" + (int)m_pathData.points[1] + "\" curvePoint=\"false\"/>\n";		
		if(m_numPoints > 2) {
			xml += "<arcpath id=\"1\" x=\"" + (int)m_pathData.points[2] + "\" y=\"" + (int)m_pathData.points[3] + "\" curvePoint=\"true\"/>\n";
			xml += "<arcpath id=\"2\" x=\"" + (int)m_pathData.points[6] + "\" y=\"" + (int)m_pathData.points[7] + "\" curvePoint=\"false\"/>\n";
		} else {
			xml += "<arcpath id=\"1\" x=\"" + (int)m_pathData.points[2] + "\" y=\"" + (int)m_pathData.points[3] + "\" curvePoint=\"false\"/>\n";
		}
		
		xml += "</arc>\n";
					
		return xml;
	}
	
	
	public void removeTokenType(int type) {
		m_arcWeights[type] = 0;
		
		int weightsum = 0;
		for(int i=0; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
			weightsum += m_arcWeights[i];
		}
		
		if(weightsum == 0)
			m_arcWeights[0] = 1;
	}
	
	public boolean openPropertiesDialog(Canvas canvas) {
		ArcPropertiesDialog dia = new ArcPropertiesDialog(canvas.getShell(), this, m_petrinet.getUniverse());		
		return dia.open();		
	} 
	
	public void delete(PetriNet pn) {
		
		m_isDeleted = true;		
		if(!m_source.isDeleted()) 	m_source.removeOutArc(this);
		if(!m_target.isDeleted())	m_target.removeInArc(this);
	}
	
	public Arc copy(PetriNet net) {
		Arc a = net.constructArc(m_source.getID(), m_target.getID());
		if(a != null) {
			a.setArcWeights(m_arcWeights);
			if(m_numPoints > 2) {
				updatePosition();
				a.drag(m_x, m_y);
			}
			
			Label l = new Label(a);
			l.drag(m_label.getX()+5, m_label.getY()+10);
		}
		return a;
	}
}
