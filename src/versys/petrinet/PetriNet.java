/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;

import versys.VersysFile;

/**
 * This class represents a Petri net in Versys.
 * @author Michael Rücker
 */
public class PetriNet {

	/**
	 * Black token identifier.
	 */
	public final static int TOKEN_BLACK 		= 0;
	
	/**
	 * The maximum number of token colors.
	 */
	public final static int TOKEN_NUM_TYPES 	= Component.s_tokenColors.length;
	
	/**
	 * The maximum number of different variables.
	 */
	public final static int NUM_VARS			= 4;
	
	/**
	 * The Petri net's universe, i.e. the currently enabled token colors. 
	 * Each boolean corresponds to a color, indicating whether it is currently
	 * enabled or not.
	 */
	private final boolean[]				m_universe;
	
	/**
	 * The net's identifier.
	 */
	private String					m_id;
	
	/**
	 * The net's current number of places.
	 */
	private int					m_numPlaces;
	
	/**
	 * The net's places.
	 */
	private final ArrayList<Place> 		m_places;
	
	/**
	 * The net's current number of transitions.
	 */
	private int					m_numTransitions;
	
	/**
	 * The net's transitions.
	 */
	private final ArrayList<Transition> 	m_transitions;
	
	/**
	 * The net's arcs.
	 */
	private final ArrayList<Arc> 			m_arcs;
	
	/**
	 * The labels of the net's components (places, transitions, arcs).
	 */
	private final ArrayList<Label> 		m_labels;
	
	/**
	 * Currently selected components.
	 */
	private final ArrayList<Component>	m_selected;
	
	/**
	 * The net's current state.
	 */
	private PNState					m_state;
	
	/**
	 * Constructs a new PetriNet instance with the specified ID.
	 * @param id the net's id
	 */
	public PetriNet(String id) {
		m_id			= id;
		
		m_numPlaces			= 0;
		m_numTransitions 	= 0;
		
		
		m_places 		= new ArrayList<>();
		m_transitions 	= new ArrayList<>();
		m_arcs 			= new ArrayList<>();
		m_labels		= new ArrayList<>();
		m_selected		= new ArrayList<>();
		m_universe		= new boolean[TOKEN_NUM_TYPES];
		m_universe[TOKEN_BLACK] = true;
		
		m_state			= PNState.PN_STATE_EDIT;	
	}
	
	/**
	 * Draws the Petri net on the given canvas.
	 * @param gc the canvas the net will be drawn on
	 */
	public void draw(GC gc) {
		for (Place p : m_places) {
			p.draw(gc);
		}

		for (Transition t : m_transitions) {
			t.draw(gc);
		}

		for (Arc a : m_arcs) {
			a.draw(gc);
		}
	}
	
	/**
	 * Adds the specified component to the Petri net. If {@code select == true}, the component is added to the net's currently selected components.
	 * @param c the component to be added
	 * @param select if {@code true}, the component is added to the net's currently selected components
	 */
	public void addComponent(Component c, boolean select) {
		
		switch(c.getType()) {
		
			case CTYPE_PLACE:
				if(c.getID() == null) {
					c.setID("p"+m_numPlaces);
					c.getLabel().setText("p"+m_numPlaces++);					
				}		
				m_places.add((Place)c);
				m_labels.add(c.getLabel());
				break;
			
			case CTYPE_TRANS:
				if(c.getID() == null) {
					c.setID("t"+m_numTransitions);
					c.getLabel().setText("t"+m_numTransitions++);
				}
				m_transitions.add((Transition)c);
				m_labels.add(c.getLabel());
				break;
				
			
			case CTYPE_ARC:
				boolean arcExists = false;
				Iterator<Arc> ait = m_arcs.iterator();
				while(!arcExists && ait.hasNext()) {
					if(ait.next().equals((Arc)c)) arcExists = true;
				}
				
				if(arcExists) {
					c.delete(this);
					
				} else {
					m_arcs.add((Arc)c);
					m_labels.add(c.getLabel());
				}
				break;
			
			default:
				c.delete(this);
		}
		c.registerPetriNet(this);
		
		if(!c.isDeleted() && select) {
			m_selected.add(c);
			c.select();
		}
	}
	
	/**
	 * Adds the specified subnet's components to the Petri net.
	 * @param subnet the subnet to be added
	 */
	public void addSubNet(PetriNet subnet) throws Exception {
		
		VersysFile vf = new VersysFile(subnet);		
		vf.parseSubNet(this, true);		
	}
	
	/**
	 * Returns a new Arc instance with the specified source and target. If source or target does not exist,
	 * or if source and target are of the same type (e.g. two places), this method returns null.
	 * @param sourceID id of the source component
	 * @param targetID id of the target component
	 * @return a new Arc instance with the specified source and target, or null if no such arc can be created
	 */
	public Arc constructArc(String sourceID, String targetID) {
		
		// Get Source Component
		Node sourceNode = null;
		
		Iterator<Place> ip = m_places.iterator();		
		while(sourceNode == null && ip.hasNext()) {
			Node c = ip.next();
			if(c.getID().equals(sourceID)) 
				sourceNode = c;
		}
		
		Iterator<Transition> it = m_transitions.iterator();
		while(sourceNode == null && it.hasNext()) {
			Node c = it.next();
			if(c.getID().equals(sourceID))
				sourceNode = c;
		}
		
		if(sourceNode == null) return null;
		
		// Get Target Component
		Node targetNode = null;
		
		ip = m_places.iterator();
		while(targetNode == null && ip.hasNext()) {
			Node c = ip.next();
			if(c.getID().equals(targetID))
				targetNode = c;
		}
		
		it = m_transitions.iterator();
		while(targetNode == null && it.hasNext()) {
			Node c = it.next();
			if(c.getID().equals(targetID))
				targetNode = c;
		}
		
		if(targetNode == null || sourceNode.getType() == targetNode.getType())
			return null;
				
		return new Arc(sourceNode, targetNode);
	}

	/**
	 * Tests whether there is a component at the specified coordinates. If so, a reference to the first found
	 * component is returned. If there is no component, null is returned.
	 * @param x the x coordinate of the point to be tested
	 * @param y the y coordinate of the point to be tested
	 * @return a reference to the first found component at the specified coordinates, or null if there is no component at these coordinates
	 */
	public Component checkSelect(int x, int y) {
		
		// If a simulation is running, check transitions first
		if(m_state != PNState.PN_STATE_EDIT) {
			for(int i = m_transitions.size()-1; i >= 0; i--) {
				Transition t = m_transitions.get(i);
				if(t.checkSelect(x, y)) return t;	
			}
		}
		
		// Check labels
		for(int i = m_labels.size()-1; i >= 0; i--) {
			Label l = m_labels.get(i);
			if(l.checkSelect(x, y)) return l;
		}
				
		// Check places
		for(int i = m_places.size()-1; i >= 0; i--) {
			Place p = m_places.get(i);
			if(p.checkSelect(x, y)) return p;
		}
		
		// If no simulation is running, check transitions now
		if(m_state == PNState.PN_STATE_EDIT) {
			for(int i = m_transitions.size()-1; i >= 0; i--) {
				Transition t = m_transitions.get(i);
				if(t.checkSelect(x, y)) return t;	
			}
		}
		
		for(int i = m_arcs.size()-1; i >= 0; i--) {
			Arc a = m_arcs.get(i);
			if(a.checkSelect(x, y)) return a;
		}
		return null;
	}
	
	/**
	 * Adds all components that lie inside the specified rectangle to the net's list of currently selected components.
	 * @param r all components inside this rectangle will be added to the net's list of currently selected components
	 * @see #checkSelect(int, int)
	 * @see #hasSelected()
	 * @see #getSelected()
	 * @see #unselect()
	 */
	public void selectComponents(Rectangle r) {

		for (Label l : m_labels) {
			if (l.checkSelect(r) && !l.isSelected()) {
				l.select();
				m_selected.add(l);
			}
		}

		for (Place p : m_places) {
			if (p.checkSelect(r) && !p.isSelected()) {
				p.select();
				m_selected.add(p);
			}
		}

		for (Transition t : m_transitions) {
			if (t.checkSelect(r) && !t.isSelected()) {
				t.select();
				m_selected.add(t);
			}
		}

		for (Arc a : m_arcs) {
			if (a.checkSelect(r) && !a.isSelected()) {
				a.select();
				m_selected.add(a);
			}
		} 
	}
	
	
	/**
	 * Changes the component's selection status. If the component is currently selected, it is unselected. If it is
	 * not selected, it becomes selected.
	 * @param c the component whose selection statis should be changed
	 * @see #m_selected
	 */
	public void toggleSelection(Component c) {
		if(m_selected.contains(c)) {
			m_selected.remove(c);
			c.unselect();
		} else {
			m_selected.add(c);
			c.select();
		}			
	}
	
	/**
	 * Returns true if the net's list of currently selected components contains at least one element.
	 * @return true if the net's list of currently selected components contains at least one element, false otherwise
	 * @see #selectComponents(Rectangle)
	 * @see #getSelected()
	 * @see #unselect() 
	 */
	public boolean hasSelected() {
		return m_selected.size() > 0;
	}
	
	/**
	 * Returns a reference to the net's list of currently selected components.
	 * @return a reference to the net's list of currently selected components
	 */
	public ArrayList<Component> getSelected() {
		return m_selected;
	}	
	
	/**
	 * Marks all currently selected components as unselected and clears the the net's list of currently selected
	 * components.
	 */
	public void unselect() {
		for (Component c : m_selected) {
			c.unselect();
		}
		m_selected.clear();		
	}
	
	/**
	 * Deletes all currently selected components and then clears the net's list of currently selected components. If
	 * at least one component is deleted, this method returns true.
	 * @return true if at least one component was deleted, false otherwise
	 */
	public boolean deleteSelected() {
		boolean change = (m_selected.size() > 0);

		for (Component c : m_selected) {
			if (!c.isDeleted()) deleteComponent(c);
		}			
		unselect();
		return change;
	}
	
	
	/**
	 * Copies the net's currently selected components.
	 * @return a copy of the net's currently selected component, treated as an individual Petri net
	 */
	public PetriNet copySelected() {
		if(m_selected.isEmpty()) return null;
		
		PetriNet copy = new PetriNet(m_id + ".selection_copy");
		
		Iterator<Component> ic = m_selected.iterator();
		while(ic.hasNext()) {
			Component c = ic.next();
			if(c.getType() == CType.CTYPE_PLACE || c.getType() == CType.CTYPE_TRANS)
				copy.addComponent( ((Node)c).copy(), false );
		}
		
		ic = m_selected.iterator();
		while(ic.hasNext()) {
			Component c = ic.next();
			if(c.getType() == CType.CTYPE_ARC) {
				Arc a = ((Arc)c).copy(copy);
				if(a != null) copy.addComponent(a, false);
			}
		}
		
		return copy;
	}
	
	/**
	 * Deletes the specified component.
	 * @param c the component to be deleted
	 */
	public void deleteComponent(Component c) {
		
		if(c == null) return;
		
		m_places.remove(c);
		m_transitions.remove(c);			
		m_arcs.remove(c);
				
		c.delete(this);
	}
	
	
	/**
	 * Moves the currently selected components to the specified coordinates. The reference component will be moved 
	 * directly to (x,y) while all other selected components will be moved such that they retain their relative 
	 * position to the reference component. 
	 * @param x the x coordinate of the drag point
	 * @param y the y coordinate of the drag point
	 * @param dragReference the reference component
	 */
	public void dragSelected(int x, int y, Component dragReference) {
		
		switch (m_selected.size()) {
			
			case 0:	return;
			
			case 1:
				m_selected.get(0).drag(x, y);
				break;
		
			default:
				
				int refX = dragReference.getX();
				int refY = dragReference.getY();

				for (Component c : m_selected) {
					if (c.getType() == CType.CTYPE_LABEL) continue;
					if (c.getType() == CType.CTYPE_ARC && !((Arc) c).isCurved()) continue;

					int dx = c.getX() - refX;
					int dy = c.getY() - refY;
					c.drag(x + dx, y + dy);
				}				
				break;
		}
	}
	
	/**
	 * Translates the entire Petri net, i.e. all its components, by the passed
	 * coordinate values
	 * @param tx translation along x-axis
	 * @param ty translation along y-axis
	 */
	public void translate(int tx, int ty) {
		// Translate places
		for (Place p : m_places) {
			p.drag(p.getX() + tx, p.getY() + ty);
		}
		
		// Translate transitions
		for (Transition t : m_transitions) {
			t.drag(t.getX() + tx, t.getY() + ty);
		}
		
		// Translate curved arcs
		for (Arc a : m_arcs) {
			if (a.isCurved()) {
				a.drag(a.getX() + tx, a.getY() + ty);
			}
		}
	}
	
	/**
	 * Returns the center point of the net.
	 * @return a point containing the center coordinates of the net
	 */
	public Point getCenter() {		
		
		Point ul = new Point(0,0);
		Point lr = new Point(0,0);
		
		Node n0;
		if(!m_places.isEmpty())				n0 = m_places.get(0);
		else if(!m_transitions.isEmpty())	n0 = m_transitions.get(0);
		else								return null;
		
		ul.x = lr.x = n0.getX();
		ul.y = lr.y = n0.getY();
		
		
		// Places
		for (Place p : m_places) {
			if (p.getX() > lr.x) lr.x = p.getX();
			if (p.getX() < ul.x) ul.x = p.getX();
			if (p.getY() > lr.y) lr.y = p.getY();
			if (p.getY() < ul.y) ul.y = p.getY();
		}
		
		// Translate transitions
		for (Transition t : m_transitions) {
			if (t.getX() > lr.x) lr.x = t.getX();
			if (t.getX() < ul.x) ul.x = t.getX();
			if (t.getY() > lr.y) lr.y = t.getY();
			if (t.getY() < ul.y) ul.y = t.getY();
		}
		
		int cx = ((lr.x-ul.x) / 2) + ul.x;
		int cy = ((lr.y-ul.y) / 2) + ul.y;
		return new Point(cx, cy);
	}
	
	/**
	 * Sets the net's universe, i.e. the currently enabled token colors.
	 * @param u the new universe
	 * @see #m_universe
	 */
	public void setUniverse(boolean[] u) {		
		for(int i=0; i < TOKEN_NUM_TYPES; i++) {
			if(m_universe[i] && !u[i]) {
				for (Place m_place : m_places) m_place.removeTokenType(i);

				for (Arc m_arc : m_arcs) m_arc.removeTokenType(i);
			}
			m_universe[i] = u[i];
		}
	}
	
	/**
	 * Returns the net's current universe.
	 * @return the net's current universe
	 * @see #m_universe
	 */
	public boolean[] getUniverse() {
		return m_universe;
	}
	
	/**
	 * Puts the net in manual simulation mode and sets the net's state to {@link PNState#PN_STATE_MANUAL_SIM}.
	 */
	public void startSimulation() {
		unselect();

		for (Place p : m_places) {
			p.startSimulation();
		}

		for (Transition t : m_transitions) {
			t.startSimulation();
		}

		for (Arc a : m_arcs) {
			a.startSimulation();
		}
		m_state = PNState.PN_STATE_MANUAL_SIM;		
	}
	
	/**
	 * Stops all simulations, both automatic and manual, and sets the nets state to {@link PNState#PN_STATE_EDIT}.
	 */
	public void stopSimulation() {
		if(m_state == PNState.PN_STATE_AUTO_SIM)
			stopAutoSimulation();

		for (Place m_place : m_places) m_place.stopSimulation();

		for (Transition m_transition : m_transitions) m_transition.stopSimulation();

		for (Arc m_arc : m_arcs) m_arc.stopSimulation();
			
		m_state = PNState.PN_STATE_EDIT;
	}
	
	/**
	 * Sets the nets state to {@link PNState#PN_STATE_AUTO_SIM}.
	 */
	public void startAutoSimulation() {
		unselect();
		m_state = PNState.PN_STATE_AUTO_SIM;
	}
	
	/**
	 * Sets the nets state to {@link PNState#PN_STATE_MANUAL_SIM}.
	 */
	public void stopAutoSimulation() {
		m_state = PNState.PN_STATE_MANUAL_SIM;
	}
	
	/**
	 * Returns the net's current state.
	 * @return the net's current state.
	 * @see PNState
	 */
	public PNState getState() {
		return m_state;
	}
	
	
	/**
	 * Attempts to fire a random transition. If there are no enabled transitions, or if the net is not in automatic 
	 * simulation mode, this method returns null. If a transition was successfully fired, this method returns that
	 * transition's label text.   
	 * @return the label text of the fired transition, or null if no transition was fired
	 * @throws Exception if something goes south
	 */
	public String fireRandomTransition() throws Exception {
		if(m_state != PNState.PN_STATE_AUTO_SIM) return null;
		
		ArrayList<Transition> enabledT = new ArrayList<>();

		for (Transition t : m_transitions) {
			if (t.isEnabled()) enabledT.add(t);
		}		
		
		int numT = enabledT.size();
		if(numT == 0) return null;
		
		int ran = (int)(Math.random()*numT);
		enabledT.get(ran).fire();
		return enabledT.get(ran).getLabel().getText();
	}
	
	/**
	 * Returns the XML representation of the net.
	 * @return the XML representation of the net
	 */
	public String toXML() {		
		// ID
		String xml = "<net id=\"" + m_id + "\" type=\"P/T net\">\n";
		
		// Token defs
		for(int i = 0; i < TOKEN_NUM_TYPES; i++) 
			xml += getXMLTokenDef(i, Component.s_tokenColors[i]);
		
		// Places
		for (Place m_place : m_places) xml += m_place.toXML();
		
		// Transitions
		for (Transition m_transition : m_transitions) xml += m_transition.toXML();
		
		// Arcs
		for (Arc m_arc : m_arcs) xml += m_arc.toXML();
		
		xml += "</net>\n";
		
		return xml;
	}

	/**
	 * Returns the net's ID.
	 * @return the net's ID.
	 */
	public String getID() {
		return m_id;
	}
	
	/** Sets the net's ID to the specified String.	 
	 * @param id the net's new ID
	 */
	public void setID(String id) {
		m_id = id;
	}
	
	/**
	 * Changes the IDs of all net components to consecutive numbers. I.e., place IDs are set to
	 * p0, p1, ..., transition IDs are set to t0, t1, ..., and arc IDs are set according to their source and target
	 * IDs: [source ID] to [target ID]. This method does not affect the components' labels.
	 */
	public void serializeComponentIDs() {
		
		m_numPlaces = 0;
		for (Place m_place : m_places) m_place.setID("p" + m_numPlaces++);
				
		m_numTransitions = 0;
		for (Transition m_transition : m_transitions) m_transition.setID("t" + m_numTransitions++);

		for (Arc m_arc : m_arcs) m_arc.updateID();
	}
	
	/**
	 * Returns an XML-Definition for the passed token id and color
	 * @param id the token id
	 * @param color the color
	 * @return an XML-Definition for the passed token id and color
	 */
	private String getXMLTokenDef(int id, Color color) {
		int r = color.getRed();
		int g = color.getGreen();
		int b = color.getBlue();
		
		return "<token id=\"" + Component.s_tokenIDs[id] + "\" enabled=\"" + m_universe[id] + "\" " +
				"red=\"" + r + "\" " +		// Save RGB for compatibility with Pipe2
				"green=\"" + g + "\" " +
				"blue=\"" + b + "\"/>\n";
	}

	/**
	 * Returns a copy of the Petri net as a new PetriNet instance.
	 * @return a copy of the net
	 * @throws Exception if an error occurs during the copy operation
	 */
	public PetriNet copy() throws Exception {
		PetriNet copy;
		VersysFile vf = new VersysFile(this);		
								
		copy = vf.parsePetriNet();		
		copy.serializeComponentIDs();
		
		return copy;
	}
}
