/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.swt.graphics.Rectangle;

/**
 * Abstract superclass of all nodes in a Petri net, i.e., places and transitions.
 * @author Michael Rücker
 *
 */
abstract public class Node extends Component {

	int m_size;
	
	final ArrayList<Arc> m_inArcs;
	final ArrayList<Arc> m_outArcs;
		
	
	/**
	 * Constructs a new Node.
	 */
	Node() {
		super();
		
		m_inArcs = new ArrayList<>();
		m_outArcs = new ArrayList<>();
	}

	/**
	 * Marks the node as deleted and deletes all its in- and outgoing arcs from the specified Petri net.
	 * @param pn the Petri net this node's arcs will be deleted from
	 */
	public void delete(PetriNet pn) {
		m_isDeleted = true;
		
		Iterator<Arc> itArc = m_inArcs.iterator();
		while(itArc.hasNext()) pn.deleteComponent(itArc.next());
		
		itArc = m_outArcs.iterator();
		while(itArc.hasNext()) pn.deleteComponent(itArc.next());
	}
	
	/**
	 * Adds the specified arc to this node's list of ingoing arcs.
	 * @param arc arc to be added to this node's list of ingoing arcs
	 */
	public void addInArc(Arc arc) {
		m_inArcs.add(arc);
	}
	
	/**
	 * Adds the specified arc to this node's list of outgoing arcs.
	 * @param arc arc to be added to this node's list of outgoing arcs
	 */
	public void addOutArc(Arc arc) {
		m_outArcs.add(arc);
	}
	
	/**
	 * Removes the specified arc from the node's list of ingoing arcs.
	 * @param arc arc to be removed from the node's list of ingoing arcs
	 */
	public void removeInArc(Arc arc) {
		m_inArcs.remove(arc);
	}
	
	/**
	 * Removes the specified arc from the node's list of outgoing arcs.
	 * @param arc arc to be removed from the node's list of outgoing arcs
	 */
	public void removeOutArc(Arc arc) {
		m_outArcs.remove(arc);
	}
	
	public int getSize() {
		return m_size;
	}
	
	public void setLabelVisible(boolean labelVisible) {
		m_label.setVisible(labelVisible);
	}
	
	public boolean isLabelVisible() {
		return m_label.isVisible();
	}
	
	public boolean checkSelect(Rectangle r) {
		return r.contains(m_x, m_y);
	}

	/**
	 * Returns a copy of the node. Copy and original will have the same properties and ID and will, for all intents and purposes, be indistinguishable.
	 * However, {@code n.copy().equals(n)} will always return {@code false}.
	 * @return a copy of the node
	 */
	public abstract Node copy();
}
