/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys.petrinet;

import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Path;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Display;

import versys.EditorTab;

/**
 * @author michael
 *
 */
public class Label extends Component {
	
	private final static PaletteData s_paletteData = new PaletteData(new RGB[] {
		s_componentColors[0].getRGB(),
		s_tokenColors[0].getRGB(),
		s_tokenColors[1].getRGB(),
		s_tokenColors[2].getRGB(),
		s_tokenColors[3].getRGB(),
		s_tokenColors[4].getRGB(),
		s_tokenColors[5].getRGB(),
		s_tokenColors[6].getRGB(),
		s_tokenColors[7].getRGB(),
	});
	
	private final Component m_component;
	
	private String m_text;
	private Image m_image;	
	
	private boolean m_isVisible;
	private boolean m_isDrawn;
	
	public Label(Component c) {
		super();
		m_type = CType.CTYPE_LABEL;
		
		m_component = c;
		c.setLabel(this);
		
		switch (c.getType()) {
		case CTYPE_PLACE:
			m_x = -25;
			m_y = 30;
			m_text = c.getID(); 
			break;
		case CTYPE_TRANS:
			m_x = -10;
			m_y = 15;
			m_text = c.getID();
			break;
		case CTYPE_ARC:
			m_x = 0;
			m_y = 10;	
			m_text = "";
		default:
			break;
		}
		m_isVisible = true;
		m_isDrawn = true;
	}
	
	public void draw(GC gc) {		
		if (!m_isVisible) return;
		
		// Place and transition label
		if(m_component.getType() != CType.CTYPE_ARC) {
			if(m_isSelected)
				gc.setForeground(s_componentColors[2]);
			else
				gc.setForeground(s_componentColors[1]);
			gc.drawString(m_text, m_component.getX()+m_x, m_component.getY()+m_y, true);
		}
			
		
		// Arc label 
		else {
			int[] weights = ((Arc)m_component).getArcWeights();
						
			int numWeights = 1;
			int largestWeight = 1;
			
			m_isDrawn = false;
			if(weights[PetriNet.TOKEN_BLACK] > 1) { 
				m_isDrawn = true;
				largestWeight = weights[PetriNet.TOKEN_BLACK];
			}
			
			for(int i=1; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
				if(weights[i] > 0) {
					m_isDrawn = true;
					numWeights++;
					if(weights[i] > largestWeight) largestWeight = weights[i];
				}
			}
									
			if(m_isDrawn) {
				int labelWidth = String.valueOf(largestWeight).length() * 10 + 20;
				int fontHeight = (int)(gc.getFont().getFontData()[0].height);				
				int labelHeight = numWeights * (fontHeight+3);
		
				if(m_image != null) m_image.dispose();
				ImageData id = new ImageData(labelWidth, labelHeight, 4, s_paletteData);
				id.transparentPixel = 0;				
				m_image = new Image(gc.getDevice(), id);

				GC gci = new GC(m_image);				
				gci.setBackground(s_componentColors[0]);
				gci.fillRectangle(0, 0, labelWidth, labelHeight);
				
				int y = 0;
				for(int i = 0; i < PetriNet.TOKEN_NUM_TYPES; i++) {
					if(weights[i] > 0) {
						int offset = 0;
						if(weights[i] > 1) {
							String factor = String.valueOf(weights[i]);
							gci.drawString(factor, 0, y, true);
							offset = factor.length() * 9;
						}
						gci.setBackground(s_tokenColors[i]);						
						gci.fillOval(offset, y+2, 10, 10);
						
						y += fontHeight + 3;
					}
				}
				gci.setBackground(s_componentColors[0]);
				for(int i = PetriNet.TOKEN_NUM_TYPES; i < PetriNet.TOKEN_NUM_TYPES+PetriNet.NUM_VARS; i++) {
					if(weights[i] > 0) {
						int offset = 0;
						if(weights[i] > 1) {
							String factor = String.valueOf(weights[i]);
							gci.drawString(factor, 0, y);
							offset = factor.length() * 9;
						}												
						gci.drawString(s_tokenIDs[i], offset, y);
						
						y += fontHeight + 3;
					}
				}
				gci.dispose();
				
				int lx = m_component.getX()+m_x;
				int ly = m_component.getY()+m_y;				
				gc.drawImage(m_image, lx, ly);
					
			}
		}
	}
	
	public boolean checkSelect(int x, int y) {
		if(!m_isVisible || !m_isDrawn) return false;
		
		if(m_component.getType() == CType.CTYPE_ARC) {
			
			Rectangle rect = m_image.getBounds();
			rect.x = m_component.getX()+m_x;
			rect.y = m_component.getY()+m_y;
			return rect.contains(x, y);
			
		} else {						
			 
			Path p = new Path(Display.getCurrent());
			p.addString(m_text, m_component.getX()+m_x, m_component.getY()+m_y, EditorTab.CANVAS_FONT);		
			float[] bounds = new float[4];
			p.getBounds(bounds);
			p.dispose();
			
			// On sone machines, getBounds() returns a width and/or height of 0.
			// As of yet, I have no idea why. Might be a bug with SWT itself.
			// Dirty fixes:
			if(bounds[2] == 0)
				bounds[2] = EditorTab.CANVAS_FONT.getFontData()[0].getHeight()*m_text.length();
			
			if(bounds[3] == 0)
				bounds[3] = EditorTab.CANVAS_FONT.getFontData()[0].getHeight();
			
			if(x < bounds[0]) return false;
			if(y < bounds[1]) return false;
			if(x > bounds[0]+bounds[2]) return false;
			return !(y > bounds[1] + bounds[3]);
		}
	}
	
	public boolean checkSelect(Rectangle r) {
		if(!m_isVisible || !m_isDrawn) return false;
		
		if(m_component.getType() == CType.CTYPE_ARC) {
			
			Rectangle rect = m_image.getBounds();
			rect.x = m_component.getX()+m_x;
			rect.y = m_component.getY()+m_y;
			return r.intersects(rect);
			
		} else {
						
			Path p = new Path(Display.getCurrent());
			p.addString(m_text, m_component.getX()+m_x, m_component.getY()+m_y, EditorTab.CANVAS_FONT);		
			float[] bounds = new float[4];
			p.getBounds(bounds);
			p.dispose();
			
			Rectangle rect = new Rectangle((int)bounds[0], (int)bounds[1], (int)bounds[2], (int)bounds[3]);			
			return r.intersects(rect);
		}	
	}
	
	@Override
	public int getX() {
		return m_component.getX() + m_x;
	}
	
	@Override
	public int getY() {
		return m_component.getY() + m_y;
	}
	
	public void drag(int x, int y) {
		m_x = x-m_component.getX()-5;
		m_y = y-m_component.getY()-10;
	}
	
	public void setText(String text) {
		m_text = text;
	}
	
	public String getText() {
		return m_text;
	}
	
	public void setVisible(boolean visible) {
		m_isVisible = visible;
	}
	
	public boolean isVisible() {
		return m_isVisible;
	}

	public boolean openPropertiesDialog(Canvas canvas) {
		return m_component.openPropertiesDialog(canvas);
	}
	
	public String toXML() {
		
		String xml = "<name visible=\""+ m_isVisible +"\">\n";
				
		// Label text
		xml += "<value>" + m_text + "</value>\n";
		
		// Label offset
		xml += "<graphics>\n" +
				"<offset x=\"" + m_x + "\" y=\"" + m_y + "\"/>\n" +
				"</graphics>";
				
		xml += "</name>\n";
		
		return xml;
	}

	@Override 
	protected void finalize() throws Throwable { 
	  try { 
	    if(m_image != null) 
	    	m_image.dispose();
	  } 
	  finally { 
	    super.finalize(); 
	  } 
	}

	public void removeTokenType(int type) {}
	public void delete(PetriNet pn) {}
}
