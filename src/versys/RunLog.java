/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.wb.swt.layout.grouplayout.GroupLayout;

/**
 * A run log, which logs occurring transitions in a table.
 * @author Michael Rücker
 *
 */
class RunLog extends Composite {
	private final Table m_log;
	private int m_step;
	/**
	 * Create the composite.
	 * @param parent the parent composite
	 *
	 */
	public RunLog(Composite parent) {
		super(parent, SWT.BORDER);
		setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		
		m_log = new Table(this, SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		m_log.setHeaderVisible(true);
		m_log.setLinesVisible(true);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(GroupLayout.LEADING)
				.add(m_log, GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(GroupLayout.LEADING)
				.add(m_log, GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
		);
		
		TableColumn tableColumn = new TableColumn(m_log, SWT.CENTER);
		tableColumn.setWidth(30);
		tableColumn.setText("#");
		
		TableColumn tblclmnTransition = new TableColumn(m_log, SWT.BORDER);
		tblclmnTransition.setWidth(60);
		tblclmnTransition.setText("Transition");		
		setLayout(groupLayout);

		m_step = 0;
	}

	/**
	 * Adds an entry to the log.
	 * @param entry the new entry
	 */
	public synchronized void log(String entry) {
		if(entry == null) return;
		
		if(!m_log.isDisposed()) {
			TableItem item = new TableItem(m_log, SWT.NONE);
			item.setText(0, ++m_step + "");
			item.setText(1, entry);
		}	
	}
	
	@Override
	public synchronized void redraw() {
		super.redraw();
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	
}
