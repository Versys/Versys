/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys;

import java.util.ArrayList;
import java.util.Objects;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.wb.swt.layout.grouplayout.GroupLayout;
import org.eclipse.wb.swt.layout.grouplayout.LayoutStyle;

import versys.dialogs.AboutDialog;
import versys.dialogs.ErrorDialog;
import versys.dialogs.NamingDialog;
import versys.dialogs.TokenColorDialog;
import versys.dialogs.UnsavedChangesDialog;
import versys.petrinet.PNState;
import versys.petrinet.PetriNet;

// ==== M A I N   T A S K S ====

// ==== Code-Related ====
// TODO: Refactor code (especially EditorTab.getPetriNet).
// TODO: Write documentation. Seriously, do it!!!

// ==== Missing Features ====
// TODO: Implement key listeners for dialogs.
// TODO: Add option for transition log? REMOVE TRANSITION LOG?
 
// ==== Known Bugs ====
// FIXME: Disable cut/paste/undo/redo in simulation modes

// ==== Other ====
// -

/**
 * The Versys main class. Responsible for creating the application window and managing tool bars and editor tabs.
 * Contains application entry point (main method).
 * @author Michael Rücker
 */
public class Versys {

	/**
	 * The application's major version number. This will be increased when the time is right. *mysterious music setting in*
	 */
	public final static short MajorVersion = 1;
	
	/**
	 * The application's minor version number. This will be increased whenever a new feature is added.
	 */
	public final static short MinorVersion = 3;
	
	/**
	 * The application's revision version number. This will be increased with every minor revision or bug fix.
	 */
	public final static short RevisionVersion = 5;
	
	/**
	 * Location of assets.
	 */
	public final static String ASSETS = "/assets/";
	
	private static Versys s_versys;
	
	private Shell m_shlVersys;
	private Display m_display;
	
	private ToolBar m_toolBar; 
	private ToolBar m_tokenBar;
	
	private ToolItem m_toolManualSim;
	private ToolItem m_toolAutoSim;
	
	private Spinner m_spinDelay;
	private Spinner m_spinAmount;
	
	private ProgressBar m_progressBar;	
	
	private Tool m_lastTool		= Tool.TOOLS_SELECT;
	private Tool m_currentTool 	= Tool.TOOLS_SELECT;

	private TabFolder m_tabFolder;
	private ArrayList<EditorTab> m_editorTabs;
	private EditorTab m_currentTab;
	
	private PetriNet m_clipboard;
	
	/**
	 * Launch the application.
	 * @param args If you don't know what this parameter does, you should probably read a Java tutorial first.
	 */
	public static void main(String[] args) {				
		
		try {			
			s_versys = new Versys();
			s_versys.open();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Open the window.
	 */
	private void open() {
		
		m_display = Display.getDefault(); 
		
		createContents();
		setMode(PNState.PN_STATE_UNDEF);
		m_shlVersys.open();
		m_shlVersys.layout();
		while (!m_shlVersys.isDisposed()) {
			if (!m_display.readAndDispatch()) {
				m_display.sleep();
			}
		}		
	}

	/**
	 * Create contents of the window, i.e., menus, tool bars and editor tab folder.
	 */
	private void createContents() {
		
		// === Shell ===
		
		m_shlVersys = new Shell();
		m_shlVersys.addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				int numTabs = m_editorTabs.size();
				for(int i=0; i<numTabs; i++) {
					m_tabFolder.setSelection(0);
					m_currentTab = m_editorTabs.get(0);
					if(!closeCurrentTab()) {
						e.doit = false;
						break;
					}						
				}
			}
		});
		m_shlVersys.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"versys-icon.png"));
		m_shlVersys.setMinimumSize(new Point(700, 300));
		m_shlVersys.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		m_shlVersys.setSize(700, 480);
		m_shlVersys.setText("Versys");
		
		m_display.addFilter(SWT.KeyDown, new Listener() {
			
			@Override
			public void handleEvent(Event e) {
				
				if(((e.stateMask & SWT.CTRL) == SWT.CTRL) && (e.keyCode == 'v')) {
					translateClipboard();
					pasteClipboard();
				}
					
				
			}
		});
		
		// === Menu Bar ===
		
		Menu menu = new Menu(m_shlVersys, SWT.BAR);
		m_shlVersys.setMenuBar(menu);
		
		MenuItem mntmDatei = new MenuItem(menu, SWT.CASCADE);
		mntmDatei.setText("Datei");
		
		Menu menu_datei = new Menu(mntmDatei);
		mntmDatei.setMenu(menu_datei);
		
		MenuItem mntmNeu = new MenuItem(menu_datei, SWT.NONE);
		mntmNeu.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"document-new-6.png"));
		mntmNeu.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				NamingDialog newDialog = new NamingDialog(m_shlVersys, EditorTab.getDefaultName());	
				String name = newDialog.open();
				
				if(!Objects.equals(name, "")) {
					m_currentTab = new EditorTab(s_versys, m_tabFolder, name);
					m_editorTabs.add(m_currentTab);
					m_tabFolder.setSelection(m_editorTabs.size()-1);
					setMode(PNState.PN_STATE_EDIT);
				//	m_currentTab.netChanged();
				}
			}
		});
		mntmNeu.setText("Neu ...\tStrg N");
		mntmNeu.setAccelerator(SWT.CTRL + 'n');
		
		MenuItem mntmffnen = new MenuItem(menu_datei, SWT.NONE);
		mntmffnen.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"document-open-7.png"));
		mntmffnen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog openDialog = new FileDialog(m_shlVersys, SWT.OPEN);
				openDialog.setFilterExtensions(new String[] {"*.xml"});
				
				String path = openDialog.open();				

				if(path != null) {
					
					EditorTab newTab = null;
					try {
						VersysFile vf = new VersysFile(path);
						newTab = new EditorTab(s_versys, m_tabFolder, vf);
						
					} catch(Exception ex) {						
						new ErrorDialog(m_shlVersys, "Datei konnte nicht geöffnet werden.", ex).open();
					}
					if(newTab != null) {
						System.out.println("newTab (open) != null");
						m_currentTab = newTab;
						m_editorTabs.add(m_currentTab);
						m_tabFolder.setSelection(m_editorTabs.size()-1);
						setMode(m_currentTab.getState());
					}
				}				
			}
		});
		mntmffnen.setText("Öffnen...\tStrg O");
		mntmffnen.setAccelerator(SWT.CTRL + 'o');
		
		new MenuItem(menu_datei, SWT.SEPARATOR);
		
		MenuItem mntmSpeichern = new MenuItem(menu_datei, SWT.NONE);
		mntmSpeichern.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"document-save-5.png"));
		mntmSpeichern.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_tabFolder.getSelectionIndex() == -1) return;
				
				String path = m_currentTab.getFilePath();
				if(path == null) {
					FileDialog saveDialog = new FileDialog(m_shlVersys, SWT.SAVE);
					saveDialog.setFilterExtensions(new String[] {"*.xml"});
					saveDialog.setFileName(m_currentTab.getName());					
					path = saveDialog.open();
				}
				if(path != null) m_currentTab.save(path);
			}
		});
		mntmSpeichern.setText("Speichern...\tStrg S");
		mntmSpeichern.setAccelerator(SWT.CTRL + 's');
		
		MenuItem mntmSpeichernUnter = new MenuItem(menu_datei, SWT.NONE);
		mntmSpeichernUnter.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_tabFolder.getSelectionIndex() == -1) return;
				
				FileDialog saveDialog = new FileDialog(m_shlVersys, SWT.SAVE);
				saveDialog.setFilterExtensions(new String[] {"*.xml"});
				saveDialog.setFileName(m_currentTab.getName());
				
				String path = saveDialog.open();
				if(path != null) m_currentTab.save(path);		
			}
		});
		mntmSpeichernUnter.setText("Speichern unter...\tStrg+Umsch S");
		mntmSpeichernUnter.setAccelerator(SWT.CTRL + SWT.SHIFT + 's');
		
		MenuItem mntmBildSpeichern = new MenuItem(menu_datei, SWT.NONE);
		mntmBildSpeichern.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {	
				if(m_tabFolder.getSelectionIndex() == -1) return;
				FileDialog saveDialog = new FileDialog(m_shlVersys, SWT.SAVE);
				saveDialog.setFilterExtensions(new String[] {"*.png"});	
				saveDialog.setFileName(m_currentTab.getName()+" (Bild)");
				m_currentTab.saveImage(saveDialog.open());
			}
		});
		mntmBildSpeichern.setText("Bild exportieren...");				
		
		new MenuItem(menu_datei, SWT.SEPARATOR);
		
		MenuItem mntmSchlieen = new MenuItem(menu_datei, SWT.NONE);
		mntmSchlieen.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"document-close-4.png"));
		mntmSchlieen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				closeCurrentTab();
			}
		});
		mntmSchlieen.setText("Schließen\tStrg W");
		mntmSchlieen.setAccelerator(SWT.CTRL + 'w');
		
		MenuItem mntmBeenden = new MenuItem(menu_datei, SWT.NONE);
		mntmBeenden.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				m_shlVersys.close();
			}
		});
		mntmBeenden.setText("Beenden");
		
		MenuItem mntmBearbeiten = new MenuItem(menu, SWT.CASCADE);
		mntmBearbeiten.setText("Bearbeiten");
		
		Menu menu_bearbeiten = new Menu(mntmBearbeiten);
		mntmBearbeiten.setMenu(menu_bearbeiten);
		
		MenuItem mntmRckgngig = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmRckgngig.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-undo-5.png"));
		mntmRckgngig.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) m_currentTab.undo();
			}
		});
		mntmRckgngig.setText("Rückgängig\tStrg Z");
		mntmRckgngig.setAccelerator(SWT.CTRL + 'z');
		
		MenuItem mntmWiederholen = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmWiederholen.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-redo-5.png"));
		mntmWiederholen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) m_currentTab.redo();
			}
		});
		mntmWiederholen.setText("Wiederholen\tStrg Y");
		mntmWiederholen.setAccelerator(SWT.CTRL + 'y');
		
		MenuItem menuItem_1 = new MenuItem(menu_bearbeiten, SWT.SEPARATOR);
		menuItem_1.setText("");
		
		MenuItem mntmAusschneiden = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmAusschneiden.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) cutSelectionToClipboard();
			}
		});
		mntmAusschneiden.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-cut-6.png"));
		mntmAusschneiden.setText("Ausschneiden\tStrg X");
		mntmAusschneiden.setAccelerator(SWT.CTRL + 'x');
		
		MenuItem mntmKopieren_1 = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmKopieren_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) copySelectionToClipboard();
			}
		});
		mntmKopieren_1.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-copy-6.png"));
		mntmKopieren_1.setText("Kopieren\tStrg C");
		mntmKopieren_1.setAccelerator(SWT.CTRL + 'c');
		
		MenuItem mntmEinfgen = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmEinfgen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) pasteClipboard();
			}
		});		
		mntmEinfgen.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-paste-6.png"));
		mntmEinfgen.setText("Einfügen\tStrg V");
	//	mntmEinfgen.setAccelerator(SWT.CTRL + 'v');
		
		MenuItem menuItem = new MenuItem(menu_bearbeiten, SWT.SEPARATOR);
		menuItem.setText("Sep");
		
		MenuItem mntmNetzKopieren = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmNetzKopieren.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) copyCurrentNet();
			}
		});
		mntmNetzKopieren.setText("Netz Kopieren");		
		
		MenuItem mntmNetzUmbenennen = new MenuItem(menu_bearbeiten, SWT.NONE);
		mntmNetzUmbenennen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(m_currentTab != null) renameCurrentNet();
			}
		});
		mntmNetzUmbenennen.setText("Netz Umbenennen");
		
		MenuItem mntmAnsichat = new MenuItem(menu, SWT.CASCADE);
		mntmAnsichat.setText("Ansicht");
		
		Menu menu_ansicht = new Menu(mntmAnsichat);
		mntmAnsichat.setMenu(menu_ansicht);
		
		MenuItem mntmVergrern = new MenuItem(menu_ansicht, SWT.NONE);
		mntmVergrern.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(m_currentTab != null) m_currentTab.zoomIn();
			}
		});
		mntmVergrern.setText("Vergrößern\tStrg +");
		mntmVergrern.setAccelerator(SWT.CTRL + '+');
		
		MenuItem mntmVerkleinern = new MenuItem(menu_ansicht, SWT.NONE);
		mntmVerkleinern.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(m_currentTab != null) m_currentTab.zoomOut();
			}
		});
		mntmVerkleinern.setText("Verkleinern\tStrg -");
		mntmVerkleinern.setAccelerator(SWT.CTRL + '-');
		
		MenuItem mntmNormalansicht = new MenuItem(menu_ansicht, SWT.NONE);
		mntmNormalansicht.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(m_currentTab != null) m_currentTab.resetZoom();
			}
		});
		mntmNormalansicht.setText("Normalansicht\tStrg 0");
		mntmNormalansicht.setAccelerator(SWT.CTRL + '0');
		
		// Help Menu
		MenuItem mntmHilfe = new MenuItem(menu, SWT.CASCADE);
		mntmHilfe.setText("Hilfe");
		
		Menu menu_hilfe = new Menu(mntmHilfe);
		mntmHilfe.setMenu(menu_hilfe);
		
		MenuItem mntmber = new MenuItem(menu_hilfe, SWT.NONE);
		mntmber.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"rating.png"));
		mntmber.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				new AboutDialog(m_shlVersys).open();
			}
		});
		mntmber.setText("Über...");
		
		
		// === Tool Bar ===
		
		m_toolBar = new ToolBar(m_shlVersys, SWT.FLAT | SWT.RIGHT);		
		
		ToolItem toolSelect = new ToolItem(m_toolBar, SWT.RADIO);
		toolSelect.setToolTipText("Auswahl");
		toolSelect.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-select.png"));
		toolSelect.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				switchTool(Tool.TOOLS_SELECT);
			}
		});
		toolSelect.setSelection(true);
		
		ToolItem toolDelete = new ToolItem(m_toolBar, SWT.RADIO);
		toolDelete.setToolTipText("Löschen");
		toolDelete.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-delete-6.png"));
		toolDelete.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				switchTool(Tool.TOOL_DELETE);
			}
		});
		
		ToolItem toolPlace = new ToolItem(m_toolBar, SWT.RADIO);
		toolPlace.setToolTipText("Platz");
		toolPlace.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"icon_place.png"));
		toolPlace.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				switchTool(Tool.TOOLS_PLACE);
			}
		});
		
		ToolItem toolTrans = new ToolItem(m_toolBar, SWT.RADIO);
		toolTrans.setToolTipText("Transition");
		toolTrans.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"icon_trans.png"));
		toolTrans.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				switchTool(Tool.TOOLS_TRANS);
			}
		});
		
		ToolItem toolArc = new ToolItem(m_toolBar, SWT.RADIO);
		toolArc.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"icon_arc.png"));
		toolArc.setToolTipText("Kante");
		toolArc.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				switchTool(Tool.TOOLS_ARCS);
			}
		});
		
		
		// === Separator ===
		
		Label sep1 = new Label(m_shlVersys, SWT.SEPARATOR | SWT.VERTICAL);
		
		m_tokenBar = new ToolBar(m_shlVersys, SWT.FLAT | SWT.RIGHT);		
		ToolItem tltmC = new ToolItem(m_tokenBar, SWT.NONE);
		tltmC.setToolTipText("Markenfarben");
		tltmC.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean[] universe;
				TokenColorDialog colorDialog = new TokenColorDialog(m_shlVersys, m_currentTab.getPetriNet().getUniverse());
				universe = colorDialog.open();
				if(universe != null)
					m_currentTab.getPetriNet().setUniverse(universe);
			}
		});
		tltmC.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"applications-graphics-4.png"));
		
		
		// === Simulation Bar ===
		
		ToolBar simBar = new ToolBar(m_shlVersys, SWT.FLAT | SWT.RIGHT);

		m_toolManualSim = new ToolItem(simBar, SWT.CHECK);
		m_toolManualSim.setToolTipText("manuelle Simulation");
		m_toolManualSim.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"arrow-right-3.png"));
		m_toolManualSim.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {			
				
				// Start manual simulation
				if(m_toolManualSim.getSelection()) {							
					
					if(m_currentTab.getState() != PNState.PN_STATE_AUTO_SIM) {
						m_currentTab.startManualSimulation();
						m_lastTool = m_currentTool;						
					} else {
						m_currentTab.stopAutoSimulation();
						m_toolAutoSim.setSelection(false);
						m_progressBar.setSelection(0);						
					}					
				//	m_currentTool = Tool.TOOLS_MANUAL_SIM;

					
				// Stop manual simulation
				} else {
					m_currentTab.stopSimulation();
					m_progressBar.setSelection(0);
					m_currentTool = m_lastTool;					
				}
				setMode(m_currentTab.getState());
				m_currentTab.redraw();
			}
		});
		
		m_toolAutoSim = new ToolItem(simBar, SWT.CHECK);
		m_toolAutoSim.setToolTipText("automatische Simulation");
		m_toolAutoSim.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"arrow-right-double-3.png"));
		m_toolAutoSim.addSelectionListener(new SelectionAdapter() {			
			public void widgetSelected(SelectionEvent e) {
				
				// Start auto simulation
				if(m_toolAutoSim.getSelection()) {
					
					if(m_currentTab.getState() != PNState.PN_STATE_MANUAL_SIM)
						m_lastTool = m_currentTool;					

					m_currentTool = Tool.TOOLS_AUTO_SIM;					
					m_currentTab.startAutoSimulation(s_versys, m_spinAmount.getSelection(), m_spinDelay.getSelection());
										
				// Stop auto simulation
				} else {				
					m_currentTab.stopSimulation();
					m_progressBar.setSelection(0);
					m_currentTool = m_lastTool;					
				}
				setMode(m_currentTab.getState());
				m_currentTab.redraw();
			}
		});
		
		Label labelNum = new Label(m_shlVersys, SWT.NONE);
		labelNum.setText("#:");
				
		m_spinAmount = new Spinner(m_shlVersys, SWT.BORDER);
		m_spinAmount.setMaximum(9999);
		m_spinAmount.setMinimum(1);
		m_spinAmount.setSelection(100);
		
		Label lblMs = new Label(m_shlVersys, SWT.NONE);
		lblMs.setText("ms:");
		
		m_spinDelay = new Spinner(m_shlVersys, SWT.BORDER);
		m_spinDelay.setMaximum(9999);
		m_spinDelay.setMinimum(10);
		m_spinDelay.setSelection(100);
		
		m_progressBar = new ProgressBar(m_shlVersys, SWT.NONE);
		
		
		// === Tab Folder ===
		
		m_tabFolder = new TabFolder(m_shlVersys, SWT.NONE);
		m_tabFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				PNState lastState = PNState.PN_STATE_UNDEF;
				if(m_currentTab != null)
					lastState = m_currentTab.getState();
				
				int index = m_tabFolder.getSelectionIndex();
				if(index >= 0 && index < m_editorTabs.size()) {					
					m_currentTab = m_editorTabs.get(m_tabFolder.getSelectionIndex());
					if(m_currentTab.getState() != PNState.PN_STATE_AUTO_SIM)
						m_progressBar.setSelection(0);
				}
				
				if(m_currentTab != null) {
					if(lastState == PNState.PN_STATE_EDIT)
						m_lastTool = m_currentTool;
					setMode(m_currentTab.getState());
				} else
					setMode(PNState.PN_STATE_UNDEF);
			}
		});
		
		m_editorTabs = new ArrayList<>();
		m_currentTab = null; 
		
		Menu menu_3 = new Menu(m_tabFolder);
		m_tabFolder.setMenu(menu_3);
		
		MenuItem mntmUmbenennen = new MenuItem(menu_3, SWT.NONE);
		mntmUmbenennen.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"fontforge.png"));
		mntmUmbenennen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				renameCurrentNet();
			}
		});
		mntmUmbenennen.setText("Netz Umbenennen");
		
		MenuItem mntmKopieren = new MenuItem(menu_3, SWT.NONE);
		mntmKopieren.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"edit-copy-6.png"));
		mntmKopieren.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				copyCurrentNet();
			}
		});
		mntmKopieren.setText("Netz Kopieren");

		MenuItem mntmSchlieen_1 = new MenuItem(menu_3, SWT.NONE);
		mntmSchlieen_1.setImage(SWTResourceManager.getImage(Versys.class, Versys.ASSETS+"document-close-4.png"));
		mntmSchlieen_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				closeCurrentTab();
			}
		});
		mntmSchlieen_1.setText("Schließen");
		
				
		// === Layout ===
		
		GroupLayout gl_shlVersys = new GroupLayout(m_shlVersys);
		gl_shlVersys.setHorizontalGroup(
			gl_shlVersys.createParallelGroup(GroupLayout.LEADING)
				.add(gl_shlVersys.createSequentialGroup()
					.addContainerGap()
					.add(gl_shlVersys.createParallelGroup(GroupLayout.LEADING)
						.add(m_tabFolder, GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE)
						.add(gl_shlVersys.createSequentialGroup()
							.add(m_toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(sep1, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(m_tokenBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.RELATED, 158, Short.MAX_VALUE)
							.add(simBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(labelNum)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(m_spinAmount, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(lblMs)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(m_spinDelay, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(LayoutStyle.RELATED)
							.add(m_progressBar, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_shlVersys.setVerticalGroup(
			gl_shlVersys.createParallelGroup(GroupLayout.LEADING)
				.add(gl_shlVersys.createSequentialGroup()
					.add(gl_shlVersys.createParallelGroup(GroupLayout.LEADING, false)
						.add(gl_shlVersys.createSequentialGroup()
							.add(12)
							.add(gl_shlVersys.createParallelGroup(GroupLayout.LEADING, false)
								.add(m_toolBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.add(simBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.add(labelNum)
								.add(m_spinAmount, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.add(lblMs)
								.add(m_spinDelay, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.add(m_progressBar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.add(gl_shlVersys.createSequentialGroup()
							.addContainerGap()
							.add(gl_shlVersys.createParallelGroup(GroupLayout.LEADING)
								.add(m_tokenBar, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.add(sep1, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))))
					.addPreferredGap(LayoutStyle.RELATED)
					.add(m_tabFolder, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		m_shlVersys.setLayout(gl_shlVersys);
	}
	
	/**
	 * Attempts to close the currently selected EditorTab. Returns {@code true} if
	 * the operation secceeded, {@code false} otherwise.
	 * @return {@code true} if the tab was closed, {@code false otherwise}
	 */
	private boolean closeCurrentTab() {
		if(m_tabFolder.getSelectionIndex() == -1) return false;		
		
		boolean close;
		if(m_currentTab.isChanged()) {
			UnsavedChangesDialog uscDialog = new UnsavedChangesDialog(m_shlVersys, m_currentTab);
			close = uscDialog.open();
			if(close) {				
				m_editorTabs.remove(m_currentTab);	
				m_currentTab.dispose();
			} 			
		
		} else {
			m_editorTabs.remove(m_currentTab);	
			m_currentTab.dispose();
			close = true;
		}
		
		int index = m_tabFolder.getSelectionIndex();
		if(index == -1)
			setMode(PNState.PN_STATE_UNDEF);
		else
			m_currentTab = m_editorTabs.get(index);
		
		return close;
	}
	
	/**
	 * Set's the application's mode according to the passed Petri net state.
	 * @param state the PNState
	 * @see PNState
	 */
	private void setMode(PNState state) {
		switch(state) {		
		case PN_STATE_EDIT:
		//	System.out.println("Setting edit mode");
			m_toolBar.setEnabled(true);
			m_tokenBar.setEnabled(true);	
			m_toolManualSim.setEnabled(true);
			m_toolManualSim.setSelection(false);
			m_toolAutoSim.setEnabled(true);
			m_toolAutoSim.setSelection(false);
			m_spinAmount.setEnabled(true);
			m_spinDelay.setEnabled(true);
			if(m_currentTool == Tool.TOOLS_MANUAL_SIM || m_currentTool == Tool.TOOLS_AUTO_SIM)
				m_currentTool = m_lastTool;
			break;
		case PN_STATE_MANUAL_SIM:
		//	System.out.println("Setting manual sim mode");
			m_toolBar.setEnabled(false);
			m_tokenBar.setEnabled(false);
			m_toolManualSim.setSelection(true);
			m_toolAutoSim.setSelection(false);
			m_currentTool = Tool.TOOLS_MANUAL_SIM;
			break;
		case PN_STATE_AUTO_SIM:
		//	System.out.println("Setting auto mode");
			m_toolBar.setEnabled(false);
			m_tokenBar.setEnabled(false);
			m_toolManualSim.setSelection(false);
			m_toolAutoSim.setSelection(true);		
			m_currentTool = Tool.TOOLS_AUTO_SIM;
			break;
		default:
		//	System.out.println("Setting null mode");
			m_toolBar.setEnabled(false);
			m_tokenBar.setEnabled(false);
			m_toolManualSim.setEnabled(false);
			m_toolAutoSim.setEnabled(false);
			m_spinAmount.setEnabled(false);
			m_spinDelay.setEnabled(false);
			m_currentTab = null;
		}
	}
	
	/**
	 * Returns the currently selected tool.
	 * @see versys.Tool
	 * @return currently selected tool
	 */
	public Tool getCurrentTool() {
		return m_currentTool;
	}

	/**
	 * Updates the progress bar according to the passed values.
	 * @param id the id of the tab that wants to update the progress bar
	 * @param value the new value of the progress bar
	 * @param max the new maximum of the progress bar
	 */
	public void updateProgressBar(int id, int value, int max) {
		if(id == m_currentTab.getID()) {
			m_progressBar.setMaximum(max);
			m_progressBar.setSelection(value);
		}
	}
	
	/**
	 * Renames the current tab's Petri net
	 */
	private void renameCurrentNet() {
		NamingDialog nameDialog = new NamingDialog(m_shlVersys, m_currentTab.getName());
		String newName = nameDialog.open();
		if(!newName.equals("")) m_currentTab.setName(newName);
	}	
	
	/**
	 * Copies the current tab's Petri net
	 */
	private void copyCurrentNet() {
		PetriNet net = m_currentTab.getPetriNet();				
		NamingDialog nd = new NamingDialog(m_shlVersys, net.getID() + " (Kopie)");
		String name = nd.open();
		if(!name.equals("")) {				
			
			EditorTab newTab = null;
			try {
				VersysFile vf = new VersysFile(m_currentTab.getPetriNet());				
				newTab = new EditorTab(s_versys, m_tabFolder, vf, name);					
			} catch(Exception ex) {				
				new ErrorDialog(m_shlVersys, "Es ist ein Fehler aufgetreten.", ex).open();
			}
			
			if(newTab != null) {
				m_currentTab = newTab;
				m_editorTabs.add(m_currentTab);
				m_tabFolder.setSelection(m_editorTabs.size()-1);			
			}
		}
	}
	
	/**
	 * Sets the currently selected tool.
	 * @param tool the tool to be selected
	 */
	private void switchTool(Tool tool) {
		m_currentTool = tool;
		m_currentTab.resetOperations();
		m_currentTab.redraw();		
	}
	
	/**
	 * Copies the current selection to the clipboard 
	 * and removes it from the Petri net.
	 */
	private void cutSelectionToClipboard() {
		if(m_currentTab.getPetriNet().hasSelected()) {
			m_clipboard = m_currentTab.getPetriNet().copySelected();
			m_currentTab.backupNet();
			m_currentTab.getPetriNet().deleteSelected();
			m_currentTab.netChanged();
			m_currentTab.redraw();
		}
	}
	
	/**
	 * Copies the current selection to the clipboard.
	 */
	private void copySelectionToClipboard() {
		if(m_currentTab.getPetriNet().hasSelected())		
			m_clipboard = m_currentTab.getPetriNet().copySelected();
	}
	
	/**
	 * Translates the content of the clipboard such that it is centered on the
	 * current mouse coordinates.
	 */
	private void translateClipboard() {
		if(m_clipboard == null || m_currentTab.getState() != PNState.PN_STATE_EDIT) 
			return;
		
		Point mp = m_currentTab.getMousePos();
		Point c = m_clipboard.getCenter();
		m_clipboard.translate(mp.x-c.x, mp.y-c.y);
	}
	
	/**
	 * Adds the current clipboard to the current Petri net.
	 */
	private void pasteClipboard() {
		if(m_clipboard == null || m_currentTab.getState() != PNState.PN_STATE_EDIT) 
			return;			
		
		m_currentTab.getPetriNet().unselect();
		m_currentTab.backupNet();
		
		//*
		try { m_currentTab.getPetriNet().addSubNet(m_clipboard); }
		catch(Exception e) {			
			new ErrorDialog(m_shlVersys, "Ein Fehler ist aufgetreten.", e).open();			
		} //*/
		m_currentTab.netChanged();
		m_currentTab.redraw();
	}
}
