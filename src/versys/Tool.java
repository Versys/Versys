/*
 * Copyright 2014 Michael Rücker
 * 
 * This file is part of Versys.
 * 
 * Versys is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Versys is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Versys. If not, see <http://www.gnu.org/licenses/>.
 */

package versys;

/**
 * Enumeration of the different tools available in Versys.
 * @author Michael Rücker
 *
 */
public enum Tool {
	/**
	 * The selection tool.
	 */
	TOOLS_SELECT,
	
	/**
	 * The deletion tool.
	 */
	TOOL_DELETE,
	
	/**
	 * The place tool.
	 */
	TOOLS_PLACE,
	
	/**
	 * The Transition tool.
	 */
	TOOLS_TRANS,
	
	/**
	 * The arc tool.
	 */
	TOOLS_ARCS,
	
	/**
	 * The manual simulation.
	 */
	TOOLS_MANUAL_SIM,
	
	/**
	 * The automated simulation.
	 */
	TOOLS_AUTO_SIM,
}
